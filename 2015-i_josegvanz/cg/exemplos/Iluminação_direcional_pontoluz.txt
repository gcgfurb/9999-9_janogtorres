{
	"metadata": {
		"formatVersion": 1,
		"type": "VisEduCG",
		"generatedBy": "ExportadorJSON",
		"objects": 9,
		"textures": 3
	},
	"urlBaseType": "relativeToVisEduCG",
	"viewPos": [316.61532462318957,325.41411535597064,1531.0613669424565],
	"viewRot": [-0.20942512576728595,0.19958337505618404,-0.006354675832053379],
	"itens": {
		"Item_10": {
			"nome": "Renderizador",
			"visible": true,
			"corLimpar": 0,
			"corFundo": 8026746,
			"verGrade": true,
			"verEixos": true,
			"tipoGrafico": 3,
			"tipo": 9,
			"filhos": {
				"Item_14": {
					"nome": "Câmera 1",
					"visible": true,
					"valorXYZ": [-200,200,700],
					"lookAt": [0,50,0],
					"near": 100,
					"far": 820,
					"fov": 55,
					"tipo": 0
				},
				"Item_11": {
					"nome": "Objeto Gráfico 1",
					"visible": true,
					"tipo": 8,
					"filhos": {
						"Item_12": {
							"nome": "Cubo 1",
							"visible": true,
							"valorXYZ": [100,100,100],
							"posicao": [0,50,0],
							"propriedadeCor": 16745006,
							"textura": "Texture_2",
							"usarTextura": false,
							"tipo": 4
						}
					}
				},
				"Item_13": {
					"nome": "Iluminação 1",
					"visible": true,
					"posicao": [0,0,400],
					"propriedadeCor": 6842473,
					"tipoLuz": 3,
					"intensidade": 1,
					"corFundoLuz": 16755200,
					"distancia": 0,
					"angulo": 0.3141592653589793,
					"expoente": 10,
					"posicaoTarget": [0,0,0],
					"tipo": 7
				},
				"Item_15": {
					"nome": "Objeto Gráfico 2",
					"visible": true,
					"tipo": 8,
					"filhos": {
						"Item_16": {
							"nome": "Cubo 2",
							"visible": true,
							"valorXYZ": [100,100,100],
							"posicao": [200,50,200],
							"propriedadeCor": 3483391,
							"textura": "Texture_4",
							"usarTextura": false,
							"tipo": 4
						}
					}
				},
				"Item_17": {
					"nome": "Objeto Gráfico 3",
					"visible": true,
					"tipo": 8,
					"filhos": {
						"Item_18": {
							"nome": "Cubo 3",
							"visible": true,
							"valorXYZ": [100,100,100],
							"posicao": [-200,50,200],
							"propriedadeCor": 1410824,
							"textura": "Texture_10",
							"usarTextura": false,
							"tipo": 4
						}
					}
				}
			}
		}
	},
	"textures": {
		"Texture_2": {
			"src": "",
			"repeat": [1,1],
			"offset": [0,0],
			"magFilter": "LinearFilter",
			"minFilter": "LinearMipMapLinearFilter",
			"anisotropy": 1
		},
		"Texture_4": {
			"src": "",
			"repeat": [1,1],
			"offset": [0,0],
			"magFilter": "LinearFilter",
			"minFilter": "LinearMipMapLinearFilter",
			"anisotropy": 1
		},
		"Texture_10": {
			"src": "",
			"repeat": [1,1],
			"offset": [0,0],
			"magFilter": "LinearFilter",
			"minFilter": "LinearMipMapLinearFilter",
			"anisotropy": 1
		}
	}
}