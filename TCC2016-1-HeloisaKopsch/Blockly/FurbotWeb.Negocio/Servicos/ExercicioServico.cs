﻿using FurbotWeb.Dominio;
using NHibernate;
using NHibernate.Linq;
using System.Linq;
using System;
using System.Collections.Generic;

namespace FurbotWeb.Negocio
{
    public class ExercicioServico
    {
        public void Adicionar(Exercicio exercicio)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Save(exercicio);
                    transaction.Commit();
                }
            }
        }

        public void AdicionarResolucao(ResolucaoExercicio resolucao)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Save(resolucao);
                    transaction.Commit();
                }
            }
        }

        public void Atualizar(Exercicio exercicio)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Update(exercicio);
                    transaction.Commit();
                }
            }
        }

        public List<ResolucaoExercicio> ObterExerciciosResolvidos(int id)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                var todosExercicios = session.Query<ResolucaoExercicio>()
                                             .Where(x => x.Usuario.Id == id)
                                             .Fetch(x => x.Exercicio);

                return todosExercicios.ToList();
            }
        }

        public void Atualizar(ResolucaoExercicio resolucao)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Update(resolucao);
                    transaction.Commit();
                }
            }
        }

        public Exercicio Obter(int id)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                return session.Query<Exercicio>().First(x => x.Id == id);
            }
        }

        public List<ResolucaoExercicio> ObterExerciciosNaoCorrigidos(int professorId, string dataResolucao = null, string nomeAluno = null, string tituloExercicio = null)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                var todosExercicios = session.Query<ResolucaoExercicio>()
                                                          .Where(x => !x.Nota.HasValue &&
                                                                       x.Exercicio.ProfessorResponsavel.Id == professorId)
                                                          .Fetch(x => x.Exercicio)
                                                          .Fetch(x => x.Usuario)
                                                          .ThenFetch(x => x.Pessoa).AsQueryable();

                if (dataResolucao != null)
                {
                    var data = DateTime.Parse(dataResolucao);
                    todosExercicios = todosExercicios.Where(x => x.DataHora.Date == data.Date);
                }

                if (nomeAluno != null)
                    todosExercicios = todosExercicios.Where(x => x.Usuario.Pessoa.Nome == nomeAluno);

                if (tituloExercicio != null)
                    todosExercicios = todosExercicios.Where(x => x.Exercicio.Titulo == tituloExercicio);

                return todosExercicios.ToList();
            }
        }

        public int ObterQuantidadeExerciciosNaoCorrigidos(int professorId)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                return session.Query<ResolucaoExercicio>().Count(x => !x.Nota.HasValue &&
                                                                 x.Exercicio.ProfessorResponsavel.Id == professorId);
            }
        }

        public Exercicio ObterExercicioParaUsuario(int usuarioId)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                var turma = session.Query<Usuario>().Where(x => x.Id == usuarioId).Select(x => x.Turma).First();

                var exerciciosResolvidos = session.Query<ResolucaoExercicio>().Where(x => x.Usuario.Id == usuarioId).Select(x => x.Exercicio);
                return session.Query<Exercicio>().FirstOrDefault(x => x.ProfessorResponsavel.Turma == turma && !exerciciosResolvidos.Contains(x));
            }
        }

        public ResolucaoExercicio ObterResolucaoExercicio(long id)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                return session.Query<ResolucaoExercicio>()
                                .Where(x => x.Id == id)
                                .Fetch(x => x.Exercicio)
                                .Fetch(x => x.Usuario)
                                .ThenFetch(x => x.Pessoa).First();
            }
        }

        public List<Exercicio> Obter(int professorId, string titulo)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                var todosExercicios = session.Query<Exercicio>()
                                             .Where(x => x.ProfessorResponsavel.Id == professorId);

                if (titulo != null)
                    todosExercicios = todosExercicios.Where(x => x.Titulo == titulo);

                return todosExercicios.ToList();
            }
        }

        public void Excluir(int id)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var exercicio = session.Query<Exercicio>().First(x => x.Id == id);
                    session.Delete(exercicio);
                    transaction.Commit();
                }
            }
        }
    }
}
