﻿using FurbotWeb.Dominio;
using NHibernate;
using NHibernate.Linq;
using System.Linq;

namespace FurbotWeb.Negocio
{
    public class UsuarioServico
    {
        public void Adicionar(Usuario usuario)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (usuario.Pessoa.Id == 0)
                        session.Save(usuario.Pessoa);

                    session.Save(usuario);
                    transaction.Commit();
                }
            }
        }

        public Usuario Obter(string login)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                return session.Query<Usuario>().Fetch(x => x.Pessoa).Where(x => x.Login == login).FirstOrDefault();
            }
        }

        public Usuario Obter(int id)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                return session.Query<Usuario>().Fetch(x => x.Pessoa).First(x => x.Id == id);
            }
        }

        public void Atualizar(Usuario usuario)
        {
            using (ISession session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Update(usuario);
                    session.Update(usuario.Pessoa);
                    transaction.Commit();
                }
            }
        }

        public object ObterEmail(string p)
        {
            throw new System.NotImplementedException();
        }


    }
}
