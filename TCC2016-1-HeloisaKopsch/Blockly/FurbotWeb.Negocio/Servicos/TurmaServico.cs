﻿using FurbotWeb.Dominio;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FurbotWeb.Negocio
{
    public class TurmaServico 
    {
        public void Adicionar(Turma turma)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    if (turma.AnoSemetre.Id == 0)
                        session.Save(turma.AnoSemetre);
                    if (turma.Disciplina.Id == 0)
                        session.Save(turma.Disciplina);

                    session.Save(turma);
                    transaction.Commit();
                }
            }
        }

        public List<Turma> ObterTodas()
        {
            using (var session = NHibernateSession.OpenSession())
            {
                return session.Query<Turma>()
                       .Fetch(x => x.AnoSemetre)
                       .Fetch(x => x.Disciplina)
                       .Where(x => x.AnoSemetre.Ano == DateTime.Now.Year).ToList();
            }
        }

        public Turma Obter(int id)
        {
            using (var session = NHibernateSession.OpenSession())
            {
                return session.Query<Turma>().First(x => x.Id == id);
            }
        }
    }
}
