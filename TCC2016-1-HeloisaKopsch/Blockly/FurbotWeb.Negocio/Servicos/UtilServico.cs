﻿
using System.Security.Cryptography;
using System.Text;
namespace FurbotWeb.Negocio
{
    public static class UtilServico
    {
        public static string GetHashSHA1(this string text)
        {
            using (var sha1Provider = new SHA1CryptoServiceProvider())
            {
                byte[] textComputed = sha1Provider.ComputeHash(Encoding.UTF8.GetBytes(text));
                var sha1Result = new StringBuilder();
                foreach (byte b in textComputed)
                {
                    sha1Result.Append(b.ToString("x2").ToLower());
                }
                return sha1Result.ToString();
            }
        }
    }
}
