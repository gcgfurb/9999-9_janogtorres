﻿using FluentNHibernate.Automapping;
using System;

namespace FurbotWeb.Negocio
{
    public class StoreConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            return type.Namespace == "FurbotWeb.Dominio";
        }
    }
}
