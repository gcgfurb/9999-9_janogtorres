﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FurbotWeb.Dominio;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace FurbotWeb.Negocio
{
    public class NHibernateSession
    {
        public static ISession OpenSession()
        {
            var dataBaseConfig = MsSqlConfiguration.MsSql2012.ConnectionString(@"Data Source=PCDAHELO\SQLEXPRESS;Integrated Security=True; initial catalog=FURBOT_WEB");
            //var dataBaseConfig = MsSqlConfiguration.MsSql2008.ConnectionString(@"Data Source=(local);Integrated Security=True; initial catalog=FURBOT_WEB");
            var storeConfig = new StoreConfiguration();

            return Fluently.Configure()
                                               .Database(dataBaseConfig)
                                               .Mappings(m => m.AutoMappings.Add(AutoMap.AssemblyOf<Pessoa>(storeConfig))
                                                                            .Add(AutoMap.AssemblyOf<Usuario>(storeConfig).Conventions.Add<EnumConvention>())
                                                                            .Add(AutoMap.AssemblyOf<Disciplina>(storeConfig))
                                                                            .Add(AutoMap.AssemblyOf<AnoSemestre>(storeConfig).Conventions.Add<EnumConvention>())
                                                                            .Add(AutoMap.AssemblyOf<Turma>(storeConfig))
                                                                            .Add(AutoMap.AssemblyOf<Exercicio>(storeConfig).Conventions.Add<EnumConvention>())
                                                                            .Add(AutoMap.AssemblyOf<ResolucaoExercicio>(storeConfig)))
                                               .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                                               .BuildSessionFactory()
                                               .OpenSession();
        }
    }

}
