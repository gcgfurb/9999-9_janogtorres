﻿using FurbotWeb.Dominio.Enum;

namespace FurbotWeb.Dominio
{
    public class Exercicio
    {
        public virtual int Id { get; set; }
        public virtual string Titulo { get; set; }
        public virtual string Enunciado { get; set; }
        public virtual string Gabarito { get; set; }
        public virtual Dificuldade Dificuldade { get; set; }
        public virtual Usuario ProfessorResponsavel { get; set; }
    }
}