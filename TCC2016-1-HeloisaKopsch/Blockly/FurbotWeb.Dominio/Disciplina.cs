﻿namespace FurbotWeb.Dominio
{
    public class Disciplina
    {
        public virtual int Id { get; set; }
        public virtual string Nome { get; set; }
    }
}