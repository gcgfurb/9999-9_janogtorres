﻿using System;

namespace FurbotWeb.Dominio
{
    public class Pessoa
    {
        public virtual int Id { get; set; }
        public virtual string Nome { get; set; }
        public virtual string CPF { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime DataNascimento { get; set; }
    }
}
