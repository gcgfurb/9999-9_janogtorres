﻿using FurbotWeb.Dominio.Enum;
namespace FurbotWeb.Dominio
{
    public class Usuario
    {
        public virtual int Id { get; set; }
        public virtual string Login { get; set; }
        public virtual string Senha { get; set; }
        public virtual bool Ativo { get; set; }
        public virtual Tipo Tipo { get; set; }
        public virtual Pessoa Pessoa { get; set; }
        public virtual Turma Turma { get; set; }
        public virtual int TentativasAcesso { get; set; }
    }
}
