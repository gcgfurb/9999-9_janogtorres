﻿namespace FurbotWeb.Dominio
{
    public class Turma
    {
        public virtual int Id { get; set; }
        public virtual AnoSemestre AnoSemetre { get; set; }
        public virtual Disciplina Disciplina { get; set; }
    }
}
