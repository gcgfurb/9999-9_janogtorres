﻿using System.ComponentModel;
namespace FurbotWeb.Dominio.Enum
{
    public enum Dificuldade
    {
        MuitoFacil = 1,
        Facil,
        Medio,
        Dificil,
        MuitoDificil 
    }
}
