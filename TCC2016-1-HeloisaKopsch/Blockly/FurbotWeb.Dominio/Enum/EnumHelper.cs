﻿namespace FurbotWeb.Dominio.Enum
{
    public static class EnumHelper
    {
        public static string Descricao(this Dificuldade dificuldade)
        {
            switch (dificuldade)
            {
                case Dificuldade.MuitoFacil:
                    return "Muito fácil";
                case Dificuldade.Facil:
                    return "Fácil";
                case Dificuldade.Medio:
                    return "Médio";
                case Dificuldade.Dificil:
                    return "Difícil";
                case Dificuldade.MuitoDificil:
                    return "Muito difícil";
                default:
                    return string.Empty;
            }
        }
    }
}
