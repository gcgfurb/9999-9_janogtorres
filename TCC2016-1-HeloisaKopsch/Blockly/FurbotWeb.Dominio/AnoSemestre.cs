﻿using FurbotWeb.Dominio.Enum;

namespace FurbotWeb.Dominio
{
    public class AnoSemestre
    {
        public virtual int Id { get; set; }

        public virtual short Ano { get; set; }

        public virtual Semestre Semetre { get; set; }
    }
}