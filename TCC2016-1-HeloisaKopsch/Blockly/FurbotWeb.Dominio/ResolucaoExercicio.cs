﻿using System;

namespace FurbotWeb.Dominio
{
    public class ResolucaoExercicio
    {
        public virtual int Id { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Exercicio Exercicio { get; set; }
        public virtual DateTime DataHora { get; set; }
        public virtual string Resposta { get; set; }
        public virtual decimal? Nota { get; set; }
    }
}
