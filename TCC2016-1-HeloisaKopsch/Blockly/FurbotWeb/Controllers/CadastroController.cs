﻿using FurbotWeb.Dominio;
using FurbotWeb.Dominio.Enum;
using FurbotWeb.Models;
using FurbotWeb.Negocio;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FurbotWeb.Controllers
{
    public class CadastroController : Controller
    {
        [HttpGet]
        public ActionResult Index(int id = 0)
        {
            var model =  new CadastroViewModel();

            if (id != 0)
            {
                var usuario = new UsuarioServico().Obter(id);
                model.Id = id;
                model.CPF = usuario.Pessoa.CPF;
                model.DataNascimento = usuario.Pessoa.DataNascimento.ToShortDateString();
                model.Email = usuario.Pessoa.Email;
                model.Login = usuario.Login;
                model.Nome = usuario.Pessoa.Nome;
                model.Senha = usuario.Senha;
                model.TurmaSelecionadaId = usuario.Turma.Id;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Cadastrar(CadastroViewModel model)
        {
            var usuarioServico = new UsuarioServico();
            var turmaServico = new TurmaServico();

            if (model.Id == 0)
            {
                var pessoa = new Pessoa()
                {
                    CPF = model.CPF,
                    DataNascimento = Convert.ToDateTime(model.DataNascimento),
                    Email = model.Email,
                    Nome = model.Nome
                };

                var usuario = new Usuario()
                {
                    Ativo = true,
                    Login = model.Login,
                    Pessoa = pessoa,
                    Senha = model.Senha,
                    TentativasAcesso = 0,
                    Tipo = Tipo.Aluno,
                    Turma = turmaServico.Obter(model.TurmaSelecionadaId)
                };

                usuarioServico.Adicionar(usuario);
                return Json("Cadastro salvo com sucesso!");
            }

            var usuarioAlterado = usuarioServico.Obter(model.Id);
            usuarioAlterado.Senha = model.Senha;
            usuarioAlterado.Turma = turmaServico.Obter(model.TurmaSelecionadaId);
            usuarioAlterado.Pessoa.CPF = model.CPF;
            usuarioAlterado.Pessoa.DataNascimento = Convert.ToDateTime(model.DataNascimento);
            usuarioAlterado.Pessoa.Email = model.Email;
            usuarioAlterado.Pessoa.Nome = model.Nome;

            usuarioServico.Atualizar(usuarioAlterado);
            return Json("Cadastro alterado com sucesso!");
        }
    }
}