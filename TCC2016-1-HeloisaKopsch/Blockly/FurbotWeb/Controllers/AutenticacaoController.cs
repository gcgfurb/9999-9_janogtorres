﻿using FurbotWeb.Dominio;
using FurbotWeb.Dominio.Enum;
using FurbotWeb.Models;
using FurbotWeb.Negocio;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace FurbotWeb.Controllers
{
    public class AutenticacaoController : Controller
    {
        private AutenticacaoViewModel _viewModel;

        [HttpGet]
        public ActionResult Index(string retornoUrl = null)
        {
            try
            {
                if (FurbotWebSession.IsAuthenticated)
                    if (FurbotWebSession.Current.IsProfessor)
                        return Redirect(RouteTable.Routes.GetVirtualPath(null, null).VirtualPath + "Professor");
                    else if (FurbotWebSession.Current.IsAluno)
                        return Redirect(RouteTable.Routes.GetVirtualPath(null, null).VirtualPath + "Aluno");

                this._viewModel = new AutenticacaoViewModel
                {
                    CaminhoRetorno = retornoUrl,
                    DivParaExibicao = "login"
                };
                return View(this._viewModel);
            }
            catch (Exception)
            {
                return Redirect("~/Erros/InternalServer");
            }
        }

        [HttpPost]
        public JsonResult Logar(AutenticacaoViewModel viewModel, string municipio = null)
        {
            object retorno;
            var usuarioServico = new UsuarioServico();
            try
            {
                var usuario = usuarioServico.Obter(viewModel.Login);

                if (usuario != null)
                {
                    if (usuario.TentativasAcesso < 4)
                    {
                        if (usuario.Ativo)
                        {
                            //GetHashSHA1()
                            if (viewModel.Senha.Equals(usuario.Senha))
                            {
                                usuario.TentativasAcesso = 0;
                                FurbotWebSession.Login(usuario);
                                retorno = new { Logado = true, CaminhoRetorno = ConfigurarPaginaRetorno(viewModel, usuario) };
                            }
                            else
                            {
                                usuario.TentativasAcesso++;

                                if (usuario.TentativasAcesso >= 4)
                                {
                                    usuario.Ativo = false;
                                    retorno =
                                        new
                                        {
                                            Modal = true,
                                            Mensagem = "Você excedeu o número de tentativas de acesso e seu acesso foi suspenso.",
                                            Solucao = "Clique em Esqueceu a senha? para redefinir a senha de acesso ao sistema."
                                        };
                                }
                                else
                                    retorno =
                                        new
                                        {
                                            Senha = true,
                                            Mensagem = "A senha está incorreta."
                                        };
                            }

                            usuarioServico.Atualizar(usuario);
                        }
                        else //usuário inativo
                            retorno =
                                new
                                {
                                    Modal = true,
                                    Mensagem = "Usuário inativo.",
                                    Solucao = "Clique em solicitar acesso ao sistema para requisitar acesso."
                                };
                    }
                    else
                    {//usuário já excedeu o número de tentativas de acesso
                        retorno =
                            new
                            {
                                Modal = true,
                                Mensagem = "Você excedeu o número de tentativas de acesso.",
                                Solucao = "Clique em Esqueceu a senha? para redefinir a senha de acesso ao sistema."
                            };
                    }
                }
                else //usuário não encontrado para o login informado
                    retorno =
                        new
                        {
                            Login = true,
                            Mensagem = "Login inválido."
                        };
            }
            catch (Exception e)
            {
                retorno = new { Logado = false, Mensagem = e.Message };
            }

            return Json(retorno);
        }

        private string ConfigurarPaginaRetorno(AutenticacaoViewModel viewModel, Usuario usuario)
        {
            string caminhoRetorno = string.Empty;
            if (viewModel.CaminhoRetorno != null)
                caminhoRetorno = viewModel.CaminhoRetorno;
            else if (usuario.Tipo == Tipo.Aluno)
            {
                caminhoRetorno = RouteTable.Routes.GetVirtualPath(null, null).VirtualPath + "Aluno";
            }
            else if (usuario.Tipo == Tipo.Professor)
                caminhoRetorno = RouteTable.Routes.GetVirtualPath(null, null).VirtualPath + "Professor";

            return caminhoRetorno;
        }

        [HttpPost]
        public ActionResult EnviarEmailSolicitacao(RecuperarSenhaViewModel viewModel)
        {
            //ValidarRecuperarSenhaViewModel(viewModel);

            //if (!ModelState.IsValid)
            //    return Json(new { Mensagem = ModelState.ToMessageValidationManager().Messages().First().Message, Status = "Falha" });

            var emailPrincipal = new UsuarioServico().ObterEmail(viewModel.Login);

            if (emailPrincipal == null)
                return PartialView("ErroAoEnviarSolicitacao");

            //var request = recuperarSenhaServico.CriarRequestDeAlteracao(viewModel.Usuario);

            //ServiceContext.GetService<IServicoEmail>().EnviarEmailRecuperarSenhaParaProcessamento(identidadeJuridica.NomeJuridico(),
            //    emailPrincipal.Endereco, ObterUrlBase(), request.UID);

            return PartialView("SolicitacaoEnviada");
        }

        [HttpGet]
        public ActionResult NovaSenha(string reset)
        {
            Guid chaveAcesso;
            if (!Guid.TryParse(reset, out chaveAcesso))
                return View("Index", new AutenticacaoViewModel { DivParaExibicao = "erroGuid" });

            //var request = recuperarSenhaServico.BuscarRequestParaAlteracao(chaveAcesso);

            //if (request == null)
            //    return View("Index", new AutenticacaoViewModel { DivParaExibicao = "erroGuid" });

            //if (!recuperarSenhaServico.RequestEstaValida(request))
            //    return View("Index", new AutenticacaoViewModel { DivParaExibicao = "solicitacaoExpirada" });

            return View("NovaSenha", new AutenticacaoViewModel { Login = "request.CpfCnpj", UID = chaveAcesso });
        }

        [HttpPost]
        public ActionResult AlterarSenha(AutenticacaoViewModel model)
        {
            if (model.UID == Guid.Empty)
                return View("Index", new AutenticacaoViewModel { DivParaExibicao = "erroGuid" });

            //var request = recuperarSenhaServico.BuscarRequestParaAlteracao(model.UID);

            //if (request == null)
            //    return View("Index", new AutenticacaoViewModel { DivParaExibicao = "erroGuid" });

            //if (!recuperarSenhaServico.RequestEstaValida(request))
            //    return View("Index", new AutenticacaoViewModel { DivParaExibicao = "solicitacaoExpirada" });

            var usuarioServico = new UsuarioServico();

            var usuario = usuarioServico.Obter(model.Login);

            usuario.Senha = model.NovaSenha.GetHashSHA1();
            if (usuario.Tipo == Tipo.Professor)
            {
                usuario.Ativo = true;
                usuario.TentativasAcesso = 0;
            }

            usuarioServico.Atualizar(usuario);
            //recuperarSenhaServico.RemoverRequestConcluida(repositorio, request);

            FurbotWebSession.Login(usuario);

            return Redirect(ConfigurarPaginaRetorno(new AutenticacaoViewModel(), usuario));
        }

        public void Sair()
        {
            FurbotWebSession.Logoff();
            Response.Redirect("~/Autenticacao");
        }

        [HttpPost]
        public ActionResult Salvar()
        {
            var anoSemestre = new AnoSemestre()
            {
                Ano = 2016,
                Semetre = Semestre.I
            };

            var disciplina = new Disciplina()
            {
                Nome = "Introdução à computação"
            };

            var turmaA = new Turma()
            {
                Disciplina = disciplina,
                AnoSemetre = anoSemestre
            };

            new TurmaServico().Adicionar(turmaA);

            var usuarioServico = new UsuarioServico();

            Pessoa mauricio = new Pessoa()
            {
                CPF = "12312312312",
                DataNascimento = new DateTime(1969, 09, 24),
                Email = "mau.capo@gmail.com",
                Nome = "Maurício Capobianco Lopes"
            };

            Usuario professor = new Usuario()
            {
                Ativo = true,
                Tipo = Tipo.Aluno,
                Login = "mlopes",
                Senha = "123",
                Pessoa = mauricio,
                Turma = turmaA
            };
            usuarioServico.Adicionar(professor);

            Pessoa heloisa = new Pessoa
            {
                CPF = "08623127951",
                DataNascimento = new DateTime(1993, 12, 11),
                Email = "manitur.heloisa@gmail.com",
                Nome = "Heloisa Kaestner Kopsch"
            };

            Usuario aluna = new Usuario()
            {
                Ativo = true,
                Tipo = Tipo.Aluno,
                Login = "hkopsch",
                Senha = "123",
                Pessoa = heloisa,
                Turma = turmaA
            };
            usuarioServico.Adicionar(aluna);

            var exercicioFacil = new Exercicio()
            {
                Dificuldade = Dificuldade.Facil,
                Enunciado = @"Faca o robo andar ate os extremos do mundo retornando a posicao inicial. \r\n
                              Cada vez que o robo atingir um dos extremos, faca-o informar que ele chegou ate aquela posicao. \r\n
                              Lembre-se de que as coordenadas sempre serao fornecidas como (x, y). \r\n
                              A primeira coluna e linhas sao a de numero ZERO.",
                Gabarito = @"while (!ehFim(DIREITA)) { 
                                 andarDireita();
                             };
                             diga('cheguei no canto superior direito);
                             while (!ehFim(ABAIXO)) {  
                                andarAbaixo();
                             };
                             diga('cheguei no canto inferior direito);
                             while (!ehFim(ESQUERDA)) { 
                                andarEsquerda();
                            };
                            diga('cheguei no canto inferior esquerdo');
                            while (!ehFim(ACIMA)) { 
                                andarAcima();
                            };"
            };
            new ExercicioServico().Adicionar(exercicioFacil);

            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}