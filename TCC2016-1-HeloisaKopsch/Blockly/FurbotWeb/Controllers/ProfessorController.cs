﻿using FurbotWeb.Dominio;
using FurbotWeb.Dominio.Enum;
using FurbotWeb.Models;
using FurbotWeb.Negocio;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FurbotWeb.Controllers
{
    [Authorize(Roles = "Professor")]
    public class ProfessorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View(new ExercicioServico().ObterQuantidadeExerciciosNaoCorrigidos(FurbotWebSession.Current.Id));
        }

        [HttpGet]
        public ActionResult Exercicio(int id = 0)
        {
            if (id != 0)
            {
                var exercicio = new ExercicioServico().Obter(id);
                return View(new ExercicioViewModel()
                {
                    Id = id,
                    Titulo = exercicio.Titulo,
                    Dificuldade = exercicio.Dificuldade.Descricao(),
                    Enunciado = exercicio.Enunciado
                });
            }

            return View(new ExercicioViewModel());
        }

        [HttpPost]
        public ActionResult CadastrarExercicio(ExercicioViewModel model)
        {
            var exercicioServico = new ExercicioServico();
            Exercicio exercicio = null;
            if (model.Id == 0)
            {
                exercicio = new Exercicio();
                PopularExercicio(model, exercicio);
                exercicioServico.Adicionar(exercicio);
                return Json("Exercício cadastrado com sucesso!");
            }

            exercicio = exercicioServico.Obter(model.Id);
            PopularExercicio(model, exercicio);
            exercicioServico.Atualizar(exercicio);

            return Json("Exercício alterado com sucesso!");
        }

        [HttpGet]
        public ActionResult CorrecaoConsulta()
        {
            var resolucaoExercicios = new ExercicioServico().ObterExerciciosNaoCorrigidos(FurbotWebSession.Current.Id)
                                                            .Select(x =>
                                                                    new ResolucaoExercicioViewModel()
                                                                    {
                                                                        Id = x.Id,
                                                                        DataResolucao = x.DataHora.ToString(),
                                                                        NomeAluno = x.Usuario.Pessoa.Nome,
                                                                        TituloExercicio = x.Exercicio.Titulo
                                                                    }).ToList();

            return View(new CorrecaoExercicioViewModel() { Resolucoes = resolucaoExercicios });
        }

        [HttpPost]
        public JsonResult FiltrarResolucoes(ResolucaoExercicioFiltroViewModel filtros)
        {
            var resolucaoExercicios = new ExercicioServico().ObterExerciciosNaoCorrigidos(FurbotWebSession.Current.Id, filtros.DataResolucao, filtros.NomeAluno, filtros.TituloExercicio)
                                                            .Select(x =>
                                                                    new ResolucaoExercicioViewModel()
                                                                    {
                                                                        Id = x.Id,
                                                                        DataResolucao = x.DataHora.ToString(),
                                                                        NomeAluno = x.Usuario.Pessoa.Nome,
                                                                        TituloExercicio = x.Exercicio.Titulo
                                                                    }).ToList();

            return Json(resolucaoExercicios);
        }

        [HttpGet]
        public ActionResult Correcao(long id)
        {
            var resolucaoExercicio = new ExercicioServico().ObterResolucaoExercicio(id);
            var viewModel = new ResolucaoExercicioViewModel()
            {
                Id = id,
                DataResolucao = resolucaoExercicio.DataHora.ToString(),
                NomeAluno = resolucaoExercicio.Usuario.Pessoa.Nome,
                TituloExercicio = resolucaoExercicio.Exercicio.Titulo,
                Enunciado = resolucaoExercicio.Exercicio.Enunciado,
                Gabarito = resolucaoExercicio.Exercicio.Gabarito,
                Resolucao = resolucaoExercicio.Resposta,
                Nota = resolucaoExercicio.Nota.ToString()
            };

            return View(viewModel);
        }

        [HttpPost]
        public JsonResult SalvarCorrecao(long id, string nota)
        {
            var servico = new ExercicioServico();
            var resolucaoExercicio = servico.ObterResolucaoExercicio(id);
            resolucaoExercicio.Nota = decimal.Parse(nota);
            servico.Atualizar(resolucaoExercicio);

            return Json("Correção salva com sucesso!");
        }

        [HttpGet]
        public ActionResult Consulta()
        {
            var exercicios = new ExercicioServico().Obter(FurbotWebSession.Current.Id, null)
                                                           .Select(x =>
                                                                   new ExercicioViewModel()
                                                                   {
                                                                       Id = x.Id,
                                                                       Titulo = x.Titulo,
                                                                       Dificuldade = x.Dificuldade.Descricao()
                                                                   }).ToList();

            return View(new ExercicioViewModel() { Exercicios = exercicios });
        }

        [HttpPost]
        public JsonResult FiltrarExercicios(ExercicioFiltroViewModel filtros)
        {
            var exercicios = new ExercicioServico().Obter(FurbotWebSession.Current.Id, filtros.Titulo)
                                                            .Select(x =>
                                                                    new ExercicioViewModel()
                                                                    {
                                                                        Id = x.Id,
                                                                        Titulo = x.Titulo,
                                                                        Dificuldade = x.Dificuldade.Descricao()
                                                                    }).ToList();

            return Json(exercicios);
        }

        [HttpPost]
        public JsonResult Excluir(int id)
        {
            try
            {
                new ExercicioServico().Excluir(id);
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        private void PopularExercicio(ExercicioViewModel model, Exercicio exercicio)
        {
            exercicio.Dificuldade = (Dificuldade)Enum.Parse(typeof(Dificuldade), model.Dificuldade);
            exercicio.Enunciado = model.Enunciado;
            exercicio.Gabarito = model.Gabarito;
            exercicio.Titulo = model.Titulo;
            exercicio.ProfessorResponsavel = new UsuarioServico().Obter(FurbotWebSession.Current.Id);
        }
    }
}