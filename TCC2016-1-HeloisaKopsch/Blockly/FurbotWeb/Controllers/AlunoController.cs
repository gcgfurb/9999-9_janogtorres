﻿using FurbotWeb.Dominio;
using FurbotWeb.Models;
using FurbotWeb.Negocio;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FurbotWeb.Controllers
{
    [Authorize(Roles = "Aluno")]
    public class AlunoController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View(ObterExercicioParaAluno());
        }

        [HttpPost]
        public JsonResult ConcluirExercicio(ExercicioViewModel exercicio)
        {
            var exercicioServico = new ExercicioServico();

            var exercicioDominio = exercicioServico.Obter(exercicio.Id);
            var resolucaoExercicio = new ResolucaoExercicio()
            {
                DataHora = DateTime.Now,
                Exercicio = exercicioDominio,
                Resposta = exercicio.Resolucao,
                Usuario = new UsuarioServico().Obter(FurbotWebSession.Current.Id)
            };

            exercicioServico.AdicionarResolucao(resolucaoExercicio);

            return Json("Concluído com sucesso!");
        }

        [HttpGet]
        public ActionResult Consulta()
        {
            var resolucaoExercicios = new ExercicioServico().ObterExerciciosResolvidos(FurbotWebSession.Current.Id)
                                                            .Select(x =>
                                                                    new ResolucaoExercicioViewModel()
                                                                    {
                                                                        Id = x.Id,
                                                                        DataResolucao = x.DataHora.ToString(),
                                                                        TituloExercicio = x.Exercicio.Titulo,
                                                                        Resolucao = x.Resposta,
                                                                        Nota = x.Nota.HasValue ? x.Nota.ToString() : "Não corrigido"
                                                                    }).ToList();

            return View(new CorrecaoExercicioViewModel() { Resolucoes = resolucaoExercicios });
        }

        [HttpGet]
        public ActionResult Visualizar(int id)
        {
            var resolucao = new ExercicioServico().ObterResolucaoExercicio(id);
            return View(new ResolucaoExercicioViewModel()
            {
                DataResolucao = resolucao.DataHora.ToString(),
                TituloExercicio = resolucao.Exercicio.Titulo,
                Enunciado = resolucao.Exercicio.Enunciado,
                Gabarito = resolucao.Exercicio.Gabarito,
                Nota = resolucao.Nota.HasValue ? resolucao.Nota.ToString() : "Não corrigido",
                Resolucao = resolucao.Resposta
            });
        }

        private ExercicioViewModel ObterExercicioParaAluno()
        {
            var exercicio = new ExercicioViewModel();
            var exercicioDominio = new ExercicioServico().ObterExercicioParaUsuario(FurbotWebSession.Current.Id);
            if (exercicioDominio != null)
            {
                exercicio.Id = exercicioDominio.Id;
                exercicio.Enunciado = exercicioDominio.Titulo + ": " + exercicioDominio.Enunciado;
            }

            return exercicio;
        }
    }
}