﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurbotWeb.Models
{
    public class RecuperarSenhaViewModel
    {
        public string Login { get; set; }
        public string Mensagem { get; set; }
        public string Status { get; set; }
    }
}