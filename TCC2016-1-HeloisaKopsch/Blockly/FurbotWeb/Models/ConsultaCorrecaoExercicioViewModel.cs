﻿using System.Collections.Generic;

namespace FurbotWeb.Models
{
    public class CorrecaoExercicioViewModel
    {
        public List<ResolucaoExercicioViewModel> Resolucoes { get; set; }
        public ResolucaoExercicioFiltroViewModel Filtros { get; set; }

        public CorrecaoExercicioViewModel()
        {
            Filtros = new ResolucaoExercicioFiltroViewModel();
        }
    }

    public class ResolucaoExercicioFiltroViewModel
    {
        public string NomeAluno { get; set; }
        public string DataResolucao { get; set; }
        public string TituloExercicio { get; set; }

    }
}