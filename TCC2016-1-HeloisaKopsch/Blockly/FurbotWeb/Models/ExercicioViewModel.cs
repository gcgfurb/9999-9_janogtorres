﻿using FurbotWeb.Dominio.Enum;
using System;
using System.Collections.Generic;

namespace FurbotWeb.Models
{
    public class ExercicioViewModel
    {
        public int Id { get; set; }

        public string Titulo { get; set; }

        public string Enunciado { get; set; }

        public string Gabarito { get; set; }

        public string Dificuldade { get; set; }

        public string Resolucao { get; set; }

        public ExercicioFiltroViewModel Filtros { get; set; }

        public List<DificuldadeViewModel> ListaDificuldades { get; set; }

        public List<ExercicioViewModel> Exercicios { get; set; }

        public ExercicioViewModel()
        {
            ListaDificuldades = new List<DificuldadeViewModel>()
            {
                new DificuldadeViewModel(Dominio.Enum.Dificuldade.MuitoFacil.ToString(), Dominio.Enum.Dificuldade.MuitoFacil.Descricao()),
                new DificuldadeViewModel(Dominio.Enum.Dificuldade.Facil.ToString(), Dominio.Enum.Dificuldade.Facil.Descricao()),
                new DificuldadeViewModel(Dominio.Enum.Dificuldade.Medio.ToString(), Dominio.Enum.Dificuldade.Medio.Descricao()),
                new DificuldadeViewModel(Dominio.Enum.Dificuldade.Dificil.ToString(), Dominio.Enum.Dificuldade.Dificil.Descricao()),
                new DificuldadeViewModel(Dominio.Enum.Dificuldade.MuitoDificil.ToString(), Dominio.Enum.Dificuldade.MuitoDificil.Descricao())
            };

            Filtros = new ExercicioFiltroViewModel();
        }
    }

    public class DificuldadeViewModel
    {
        public string Valor { get; set; }

        public string Descricao { get; set; }

        public DificuldadeViewModel()
        {

        }

        public DificuldadeViewModel(string valor, string descricao)
        {
            Valor = valor;
            Descricao = descricao;
        }
    }

    public class ExercicioFiltroViewModel
    {
        public string Titulo { get; set; }
    }
}