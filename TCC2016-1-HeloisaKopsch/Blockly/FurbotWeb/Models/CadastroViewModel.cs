﻿using System.Linq;
using FurbotWeb.Negocio;
using System;
using System.Collections.Generic;

namespace FurbotWeb.Models
{
    public class CadastroViewModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string CPF { get; set; }

        public string Email { get; set; }

        public string DataNascimento { get; set; }

        public int TurmaSelecionadaId { get; set; }

        public List<TurmaViewModel> ListaTurma { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        public CadastroViewModel()
        {
            ListaTurma = new TurmaServico()
                        .ObterTodas()
                        .Select(x => new TurmaViewModel(x.Id, string.Concat(x.AnoSemetre.Ano, "/", x.AnoSemetre.Semetre, " - ", x.Disciplina.Nome)))
                        .ToList();
        }
    }

    public class TurmaViewModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public TurmaViewModel()
        {

        }

        public TurmaViewModel(int id, string descricao)
        {
            Id = id;
            Descricao = descricao;
        }
    }
}