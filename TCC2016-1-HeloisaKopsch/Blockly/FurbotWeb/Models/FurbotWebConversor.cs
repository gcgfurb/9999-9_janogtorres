﻿using FurbotWeb.Dominio;
using System.Security.Principal;

namespace FurbotWeb.Models
{
    public static class FurbotWebConversor
    {
        public static FurbotWebPrincipalViewModel Criar(Usuario usuario)
        {
            var principalViewModel = new FurbotWebPrincipalViewModel()
            {
                Id = usuario.Id,
                Nome = usuario.Pessoa.Nome,
                Role = usuario.Tipo.ToString()
            };

            return principalViewModel;
        }

        public static FurbotWebPrincipal ConverterViewModelParaPrincipal(IIdentity identity, FurbotWebPrincipalViewModel principalViewModel)
        {
            var principal = new FurbotWebPrincipal(identity, principalViewModel.Role);
            principal.Id = principalViewModel.Id;
            principal.Nome = principalViewModel.Nome;

            return principal;
        }
    }
}