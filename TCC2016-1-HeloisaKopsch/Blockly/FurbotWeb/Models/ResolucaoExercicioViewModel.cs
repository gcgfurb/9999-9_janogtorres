﻿namespace FurbotWeb.Models
{
    public class ResolucaoExercicioViewModel
    {
        public long Id { get; set; }
        public string NomeAluno { get; set; }
        public string DataResolucao { get; set; }
        public string TituloExercicio { get; set; }
        public string Enunciado { get; set; }
        public string Gabarito { get; set; }
        public string Resolucao { get; set; }
        public string Nota { get; set; }
    }
}