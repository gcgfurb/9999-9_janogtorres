﻿
using FurbotWeb.Dominio;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Security;
namespace FurbotWeb.Models
{
    public static class FurbotWebSession
    {
        private static readonly string CookieName = FormsAuthentication.FormsCookieName;

        public static FurbotWebPrincipal Current
        {
            get
            {
                return Context.User as FurbotWebPrincipal;
            }
        }

        private static HttpContext Context
        {
            get
            {
                return HttpContext.Current;
            }
        }

        public static bool IsAuthenticated
        {
            get
            {
                return Context.User != null && (Context.User as FurbotWebPrincipal) != null;
            }
        }

        public static void LoadSession()
        {
            if (IsAuthenticated)
                return;

            if (Context.User != null && Context.User.Identity != null)
            {
                var identity = Context.User.Identity as FormsIdentity;

                var principalViewModel = JsonConvert.DeserializeObject<FurbotWebPrincipalViewModel>(identity.Ticket.UserData);

                Context.User = FurbotWebConversor.ConverterViewModelParaPrincipal(identity, principalViewModel);
            }
        }

        public static void Login(Usuario usuario)
        {
            DateTime expiredDate = DateTime.Now.AddMinutes(30);

            var principalViewModel = FurbotWebConversor.Criar(usuario);
            var principalSerializado = JsonConvert.SerializeObject(principalViewModel, Formatting.None);

            var authenticationTicket = new FormsAuthenticationTicket(1,
                                                                     usuario.Pessoa.Nome,
                                                                     DateTime.Now,
                                                                     expiredDate,
                                                                     false,
                                                                     principalSerializado);

            var encryptedTicket = FormsAuthentication.Encrypt(authenticationTicket);

            Context.Response.Cookies.Add(new HttpCookie(CookieName, encryptedTicket));
        }

        public static void Logoff()
        {
            var cookie = Context.Response.Cookies.Get(CookieName);
            cookie.Expires = DateTime.Now.AddDays(-1);
        }
    }
}