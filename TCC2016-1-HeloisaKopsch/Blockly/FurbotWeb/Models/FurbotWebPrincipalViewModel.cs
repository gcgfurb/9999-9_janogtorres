﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace FurbotWeb.Models
{
    [Serializable]
    public class FurbotWebPrincipalViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Role { get; set; }        
    }
}