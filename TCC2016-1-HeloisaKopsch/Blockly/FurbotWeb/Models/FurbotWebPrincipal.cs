﻿using FurbotWeb.Dominio.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FurbotWeb.Models
{
    public class FurbotWebPrincipal : IPrincipal
    {
        private readonly string ProfessorRoleName =  Tipo.Professor.ToString();

        private readonly string AlunoRoleName = Tipo.Aluno.ToString();

        public int Id { get; set; }

        public string Nome { get; set; }

        public string RoleName { get; private set; }

        public IIdentity Identity { get; private set; }

        public bool IsProfessor { get { return IsInRole(ProfessorRoleName); } }

        public bool IsAluno { get { return IsInRole(AlunoRoleName); } }

        public FurbotWebPrincipal(IIdentity identity, string role)
        {
            Identity = identity;
            RoleName = role;
        }       

        public bool IsInRole(string role)
        {
            return RoleName.Equals(role, System.StringComparison.OrdinalIgnoreCase);
        }
    }
}