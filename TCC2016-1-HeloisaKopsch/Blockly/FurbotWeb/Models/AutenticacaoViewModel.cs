﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FurbotWeb.Models
{
    public class AutenticacaoViewModel
    {
        public string Login { get; set; }

        public string Senha { get; set; }

        public string DivParaExibicao { get; set; }

        public Guid UID { get; set; }

        [Required(ErrorMessage = "A nova senha deve ser informada.")]
        public string NovaSenha { get; set; }

        [Required(ErrorMessage = "A confirmação da nova senha deve ser informada.")]
        [Compare("NovaSenha", ErrorMessage = "A confirmação de senha não confere com a nova senha informada.")]
        public string ConfirmacaoSenha { get; set; }

        public string CaminhoRetorno { get; set; }
    }
}