﻿function inicializarExercicio() {
    buildGame(5);

    var alien = Alien();
    adicionarObjetoNoMundoEmbaixo(alien);
}

function PropriedadesObjeto() { }
PropriedadesObjeto.prototype = new Component();
PropriedadesObjeto.prototype.getSystems = function () {
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, LogicSystem);
    return systems;
};
PropriedadesObjeto.prototype.getTag = function () {
    return "PROPRIEDADES_COMPONENT";
};
PropriedadesObjeto.prototype.souDoTipo = 'ROBÔ';
PropriedadesObjeto.prototype.ehDependenteEnergia = false;
PropriedadesObjeto.prototype.getMaxEnergia = 100;
PropriedadesObjeto.prototype.energia = 100;
PropriedadesObjeto.prototype.jahExplodiu = false;
PropriedadesObjeto.prototype.ehBloqueado = false;

function Colisao() { }
Colisao.prototype = new Component();
Colisao.prototype.getSystems = function () {
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, LogicSystem);
    return systems;
}
Colisao.prototype.getTag = function () {
    return "COLISAO_COMPONENT";
};
Colisao.prototype.onCollide = function (otherGameObject) {
    layer.removeGameObject(otherGameObject);
    adicionarExplosao();
};

function adicionarExplosao() {
    var explosao = new BoxObject().initialize(0, 0, 0, areaMapa, areaMapa, 0);
    ComponentUtils.addComponent(explosao, new ImageRenderComponent().initialize(AssetStore.getAsset("EXPLOSAO").getAssetInstance(), false, "HORIZONTAL"));
    layer.addGameObject(explosao);

    explosao.setPosition(player.getCenterX(), player.getCenterY());

    layer.removeGameObject(player);
    ComponentUtils.removeComponent(player, ComponentUtils.getComponent(player, "COLISAO_COMPONENT"));
}

function buildGame(tamanhoMap) {
    areaMapa = 49;
    tamanhoMapa = tamanhoMap;

    Game.loadAPI();

    AssetStore.addAsset(new Asset().initialize("PLAYER", "Images/r2d2-iconTransparent.png", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("BACKGROUND_IMAGE", "Images/map.png", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("ALIEN", "Images/alien.gif", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("PAREDE", "Images/wall.png", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("ALIENMOVEL", "Images/alienmovel.gif", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("TESOURO", "Images/tesouro.gif", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("PROJETIL", "Images/shell.gif", "IMAGE"));
    AssetStore.addAsset(new Asset().initialize("EXPLOSAO", "Images/explosao.gif", "IMAGE"));

    ComponentUtils.addComponent(Game, new MoveCameraComponent().initialize(65, 68, 87, 83, 33, 34, 72, 75, 20, 0.1, 5));

    player = new BoxObject().initialize(0, 0, 0, areaMapa, areaMapa, 0);
    ComponentUtils.addComponent(player, new RigidBodyComponent().initialize(0, 1, false, false, 0.2, true));
    ComponentUtils.addComponent(player, new ImageRenderComponent().initialize(AssetStore.getAsset("PLAYER").getAssetInstance(), false, "HORIZONTAL"));
    ComponentUtils.addComponent(player, new PropriedadesObjeto().initialize());
    ComponentUtils.addComponent(player, new Colisao().initialize());

    var scene = new Scene().initialize(-400, -400, 400, 400);
    layer = new Layer().initialize();
    layer.setGravity(0);
    layer.addGameObject(player);

    var layer2 = new Layer().initialize();

    for (var i = 0; i < tamanhoMap; i++) {
        for (var j = 0; j < tamanhoMap; j++) {
            var map = new GameObject().initialize(j * areaMapa, i * areaMapa, 0, areaMapa, areaMapa, 0);
            ComponentUtils.addComponent(map, new ImageRenderComponent().initialize(AssetStore.getAsset("BACKGROUND_IMAGE").getAssetInstance(), false, "HORIZONTAL"));
            layer2.addGameObject(map);
        }
    }

    scene.addLayer(layer2);
    scene.addLayer(layer);

    Game.init(document.getElementById("gameCanvas"), scene);

    Game.camera.centerPoint.x = 100;
    Game.camera.centerPoint.y = 100;
}

function resolucaoExercicioViewModel() {
    var self = this;
    ko.mapping.fromJS(jsonViewModel, {}, self);

    self.gerarCodigo = function () {
        alert(Blockly.JavaScript.workspaceToCode(workspace));
    };

    self.executar = function () {
        var code = Blockly.JavaScript.workspaceToCode(workspace);
        myInterpreter = new Interpreter(code, initApi);
        nextStep();
    };

    self.depurar = function () {
        //Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1);\n';
        //Blockly.JavaScript.addReservedWords('highlightBlock');
        //self.executar();
    };

    self.concluir = function () {
        self.Resolucao(Blockly.JavaScript.workspaceToCode(workspace));
        $.ajax({
            url: urlPadrao + 'ConcluirExercicio',
            type: 'POST',
            data: ko.toJSON(self),
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function (retorno) {
            alert(retorno);
            location.reload();
        });
    };

    self.reiniciar = function () {
        inicializarExercicio();
    };
}

function nextStep() {
    if (myInterpreter.step()) {
        window.setTimeout(nextStep, 100 - $("#velocidade").slider("value"));
    }
}

function initApi(interpreter, scope) {
    var wrapper = function () {
        return interpreter.createPrimitive(andarDireita());
    };
    interpreter.setProperty(scope, 'andarDireita',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(andarEsquerda());
    };
    interpreter.setProperty(scope, 'andarEsquerda',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(andarAcima());
    };
    interpreter.setProperty(scope, 'andarAcima',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(andarAbaixo());
    };
    interpreter.setProperty(scope, 'andarAbaixo',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (text_obj_x, text_obj_y) {
        return interpreter.createPrimitive(mudarPosicao(text_obj_x, text_obj_y));
    };
    interpreter.setProperty(scope, 'mudarPosicao',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (value_texto) {
        return interpreter.createPrimitive(diga(value_texto));
    };
    interpreter.setProperty(scope, 'diga',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (dropdown_direcao) {
        return interpreter.createPrimitive(ehVazio(dropdown_direcao));
    };
    interpreter.setProperty(scope, 'ehVazio',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (dropdown_direcao) {
        return interpreter.createPrimitive(ehFim(dropdown_direcao));
    };
    interpreter.setProperty(scope, 'ehFim',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(getX());
    };
    interpreter.setProperty(scope, 'getX',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(getY());
    };
    interpreter.setProperty(scope, 'getY',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (dropdown_direcao) {
        return interpreter.createPrimitive(getObjeto(dropdown_direcao));
    };
    interpreter.setProperty(scope, 'getObjeto',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (dropdown_obj_tipo, dropdown_direcao) {
        return interpreter.createPrimitive(ehObjetoDoMundoTipo(dropdown_obj_tipo, dropdown_direcao));
    };
    interpreter.setProperty(scope, 'ehObjetoDoMundoTipo',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo, dropdown_direcao) {
        return interpreter.createPrimitive(adicionarObjetoNoMundo(variable_obj_mundo, dropdown_direcao));
    };
    interpreter.setProperty(scope, 'adicionarObjetoNoMundo',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo, text_obj_x, text_obj_y) {
        return interpreter.createPrimitive(adicionarObjetoNoMundoXY(variable_obj_mundo, text_obj_x, text_obj_y));
    };
    interpreter.setProperty(scope, 'adicionarObjetoNoMundoXY',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(removerObjetoDoMundo(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'removerObjetoDoMundo',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (text_obj_x, text_obj_y) {
        return interpreter.createPrimitive(getObjetoXY(text_obj_x, text_obj_y));
    };
    interpreter.setProperty(scope, 'getObjetoXY',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(ehDependenteEnergia());
    };
    interpreter.setProperty(scope, 'ehDependenteEnergia',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(getMaxEnergia());
    };
    interpreter.setProperty(scope, 'getMaxEnergia',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(getEnergia());
    };
    interpreter.setProperty(scope, 'getEnergia',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (text_segundos) {
        return interpreter.createPrimitive(esperar(text_segundos));
    };
    interpreter.setProperty(scope, 'esperar',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(esperarAlguem());
    };
    interpreter.setProperty(scope, 'esperarAlguem',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (text_max) {
        return interpreter.createPrimitive(sortear(text_max));
    };
    interpreter.setProperty(scope, 'sortear',
        interpreter.createNativeFunction(wrapper));

    wrapper = function () {
        return interpreter.createPrimitive(jahExplodiu());
    };
    interpreter.setProperty(scope, 'jahExplodiu',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(adicionarObjetoNoMundoEmbaixo(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'adicionarObjetoNoMundoEmbaixo',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(ehBloqueado(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'ehBloqueado',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(bloquear(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'bloquear',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(desbloquear(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'desbloquear',
        interpreter.createNativeFunction(wrapper));

    wrapper = function (variable_obj_mundo) {
        return interpreter.createPrimitive(getSouDoTipo(variable_obj_mundo));
    };
    interpreter.setProperty(scope, 'getSouDoTipo',
        interpreter.createNativeFunction(wrapper));

    // Add an API function for highlighting blocks.
    //var wrapper = function (id) {
    //    id = id ? id.toString() : '';
    //    return interpreter.createPrimitive(workspace.highlightBlock(id));
    //};
    //interpreter.setProperty(scope, 'highlightBlock',
    //    interpreter.createNativeFunction(wrapper));
}