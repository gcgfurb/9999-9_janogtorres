﻿function Alien() {
    return obterObjeto("ALIEN");
}

function Parede() {
    return obterObjeto("PAREDE");
}

function AlienMovel() {
    return obterObjeto("ALIENMOVEL");
}

function Tesouro() {
    return obterObjeto("TESOURO");
}

function Projetil() {
    return obterObjeto("PROJETIL");
}

function obterObjeto(tipo) {
    var objeto = new BoxObject().initialize(0, 0, 0, areaMapa - 1, areaMapa - 1, 0);
    ComponentUtils.addComponent(objeto, new RigidBodyComponent().initialize(0, 1, false, false, 0.2));
    ComponentUtils.addComponent(objeto, new ImageRenderComponent().initialize(AssetStore.getAsset(tipo).getAssetInstance(), false, "HORIZONTAL"));
    ComponentUtils.addComponent(objeto, new PropriedadesObjeto().initialize());
    var propriedades = ComponentUtils.getComponent(objeto, "PROPRIEDADES_COMPONENT");
    propriedades.souDoTipo = tipo;
    return objeto;
}

//INÍCIO COMANDOS MOVIMENTAÇÃO
function andarDireita() {
    player.addMove(areaMapa, 0);
}

function andarEsquerda() {
    player.addMove(-areaMapa, 0);
}

function andarAcima() {
    player.addMove(0, -areaMapa);
}

function andarAbaixo() {
    player.addMove(0, areaMapa);
}

function mudarPosicao(text_obj_x, text_obj_y) {
    player.setPosition(text_obj_x * areaMapa, text_obj_y * areaMapa);
}
//FIM COMANDOS MOVIMENTAÇÃO

//INÍCIO COMANDOS COMUNICAÇÃO
function diga(value_texto) {
    alert(value_texto);
}
//FIM COMANDOS COMUNICAÇÃO

//INÍCIO COMANDOS AVALIAÇÃO DO MUNDO
function ehVazio(dropdown_direcao) {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();
    if (dropdown_direcao == "DIREITA") {
        var retorno = layer.queryGameObjects(posicaoX + areaMapa, posicaoY, 2, 2, 20);
        return retorno == null || retorno[0] == null;
    }
    else if (dropdown_direcao == "ESQUERDA") {
        var retorno = layer.queryGameObjects(posicaoX - areaMapa, posicaoY, 2, 2, 20);
        return retorno == null || retorno[0] == null;
    }
    else if (dropdown_direcao == "ACIMA") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY - areaMapa, 2, 2, 20);
        return retorno == null || retorno[0] == null;
    }
    else if (dropdown_direcao == "ABAIXO") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY + areaMapa, 2, 2, 20);
        return retorno == null || retorno[0] == null;
    }
    else if (dropdown_direcao == "AQUIMESMO") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY, 2, 2, 20);
        return retorno[1] == null;
    }
}

function ehFim(dropdown_direcao) {
    var posicaoX = player.getCenterX() / areaMapa;
    var posicaoY = player.getCenterY() / areaMapa;
    if (dropdown_direcao == "DIREITA") {
        return posicaoX + 1 == tamanhoMapa;
    }
    else if (dropdown_direcao == "ESQUERDA") {
        return posicaoX == 0;
    }
    else if (dropdown_direcao == "ACIMA") {
        return posicaoY == 0;
    }
    else if (dropdown_direcao == "ABAIXO") {
        return posicaoY + 1 == tamanhoMapa;
    }
    else if (dropdown_direcao == "AQUIMESMO") {
        return false;
    }
}
//FIM COMANDOS AVALIAÇÃO DO MUNDO

//INÍCIO COMANDOS OBTENÇÃO DE COORDENADAS
function getX() {
    return player.getCenterX() / 49;
}

function getY() {
    return player.getCenterY() / 49;
}
//FIM COMANDOS OBTENÇÃO DE COORDENADAS

//INÍCIO COMANDOS MANIPULAÇÃO DE OBJETOS
function getObjeto(dropdown_direcao) {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();
    if (dropdown_direcao == "DIREITA") {
        var retorno = layer.queryGameObjects(posicaoX + areaMapa, posicaoY, 2, 2, 20);
        return retorno[0];
    }
    else if (dropdown_direcao == "ESQUERDA") {
        var retorno = layer.queryGameObjects(posicaoX - areaMapa, posicaoY, 2, 2, 20);
        return retorno[0];
    }
    else if (dropdown_direcao == "ACIMA") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY - areaMapa, 2, 2, 20);
        return retorno[0];
    }
    else if (dropdown_direcao == "ABAIXO") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY + areaMapa, 2, 2, 20);
        return retorno[0];
    }
    else if (dropdown_direcao == "AQUIMESMO") {
        var retorno = layer.queryGameObjects(posicaoX, posicaoY, 2, 2, 20);
        return retorno[1];
    }
}

function ehObjetoDoMundoTipo(dropdown_obj_tipo, dropdown_direcao) {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();
    var objeto = null;
    if (dropdown_direcao == "DIREITA") {
        objeto = layer.queryGameObjects(posicaoX + areaMapa, posicaoY, 2, 2, 20);
        var propriedades = ComponentUtils.getComponent(objeto[0], "PROPRIEDADES_COMPONENT");
        return propriedades.souDoTipo == dropdown_obj_tipo;
    }
    else if (dropdown_direcao == "ESQUERDA") {
        objeto = layer.queryGameObjects(posicaoX - areaMapa, posicaoY, 2, 2, 20);
        var propriedades = ComponentUtils.getComponent(objeto[0], "PROPRIEDADES_COMPONENT");
        return propriedades.souDoTipo == dropdown_obj_tipo;
    }
    else if (dropdown_direcao == "ACIMA") {
        objeto = layer.queryGameObjects(posicaoX, posicaoY - areaMapa, 2, 2, 20);
        var propriedades = ComponentUtils.getComponent(objeto[0], "PROPRIEDADES_COMPONENT");
        return propriedades.souDoTipo == dropdown_obj_tipo;
    }
    else if (dropdown_direcao == "ABAIXO") {
        objeto = layer.queryGameObjects(posicaoX, posicaoY + areaMapa, 2, 2, 20);
        var propriedades = ComponentUtils.getComponent(objeto[0], "PROPRIEDADES_COMPONENT");
        return propriedades.souDoTipo == dropdown_obj_tipo;
    }
    else if (dropdown_direcao == "AQUIMESMO") {
        objeto = layer.queryGameObjects(posicaoX, posicaoY, 2, 2, 20);
        var propriedades = ComponentUtils.getComponent(objeto[1], "PROPRIEDADES_COMPONENT");
        return propriedades.souDoTipo == dropdown_obj_tipo;
    }
}

function adicionarObjetoNoMundo(variable_obj_mundo, dropdown_direcao) {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();
    layer.addGameObject(variable_obj_mundo);
    if (dropdown_direcao == "DIREITA") {
        variable_obj_mundo.setPosition(posicaoX + areaMapa, posicaoY);
    }
    else if (dropdown_direcao == "ESQUERDA") {
        variable_obj_mundo.setPosition(posicaoX - areaMapa, posicaoY);
    }
    else if (dropdown_direcao == "ACIMA") {
        variable_obj_mundo.setPosition(posicaoX, posicaoY - areaMapa);
    }
    else if (dropdown_direcao == "ABAIXO") {
        variable_obj_mundo.setPosition(posicaoX, posicaoY + areaMapa);
    }
    else if (dropdown_direcao == "AQUIMESMO") {
        variable_obj_mundo.setPosition(posicaoX, posicaoY);
    }
}

function adicionarObjetoNoMundoXY(variable_obj_mundo, text_obj_x, text_obj_y) {
    layer.addGameObject(variable_obj_mundo);
    variable_obj_mundo.setPosition(text_obj_x * areaMapa, text_obj_y * areaMapa);
}

function removerObjetoDoMundo(variable_obj_mundo) {
    layer.removeGameObject(variable_obj_mundo);
}

function getObjetoXY(text_obj_x, text_obj_y) {
    var retorno = layer.queryGameObjects(text_obj_x * areaMapa, text_obj_y * areaMapa, 2, 2, 20);
    return retorno[0];
}
//FIM COMANDOS MANIPULAÇÃO DE OBJETOS

//INÍCIO OUTROS COMANDOS
function ehDependenteEnergia() {
    var propriedades = ComponentUtils.getComponent(player, "PROPRIEDADES_COMPONENT");
    return propriedades.ehDependenteEnergia;
}

function getMaxEnergia() {
    var propriedades = ComponentUtils.getComponent(player, "PROPRIEDADES_COMPONENT");
    return propriedades.getMaxEnergia;
}

function getEnergia() {
    var propriedades = ComponentUtils.getComponent(player, "PROPRIEDADES_COMPONENT");
    return propriedades.energia;
}

function esperar(text_segundos) {
    setTimeout(function () { }, text_segundos * 1000);
}

function esperarAlguem() {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();

    var objeto = [];
    for (var conta = 0; objeto[1] == null && conta < 5; conta++)
        setTimeout(function () {
            objeto = layer.queryGameObjects(posicaoX, posicaoY, 0, 2, 2, 20);
        }, 1000);

    return objeto[1];
}

function sortear(text_max) {
    return Math.floor((Math.random() * text_max) + 1);
}

function jahExplodiu() {
    var propriedades = ComponentUtils.getComponent(player, "PROPRIEDADES_COMPONENT");
    return propriedades.jahExplodiu;
}

function adicionarObjetoNoMundoEmbaixo(variable_obj_mundo) {
    var posicaoX = player.getCenterX();
    var posicaoY = player.getCenterY();
    layer.addGameObject(variable_obj_mundo);
    variable_obj_mundo.setPosition(posicaoX, posicaoY + areaMapa);
}

function ehBloqueado(variable_obj_mundo) {
    var propriedades = ComponentUtils.getComponent(variable_obj_mundo, "PROPRIEDADES_COMPONENT");
    return propriedades.ehBloqueado;
}

function bloquear(variable_obj_mundo) {
    var propriedades = ComponentUtils.getComponent(variable_obj_mundo, "PROPRIEDADES_COMPONENT");
    propriedades.ehBloqueado = true;
}

function desbloquear(variable_obj_mundo) {
    var propriedades = ComponentUtils.getComponent(variable_obj_mundo, "PROPRIEDADES_COMPONENT");
    propriedades.ehBloqueado = false;
}

function getSouDoTipo(variable_obj_mundo) {
    var propriedades = ComponentUtils.getComponent(variable_obj_mundo, "PROPRIEDADES_COMPONENT");
    return propriedades.souDoTipo;
}
//FIM OUTROS COMANDO