﻿function mensagemValidacaoCampos(nomeElemento, mensagem, formulario) {
    $('#' + nomeElemento).addClass('input-validation-error');
    var elementoSpan = formulario.find('[data-valmsg-for$="' + nomeElemento + '"]');
    elementoSpan.addClass('field-validation-error');
    elementoSpan.html('<span for="' + nomeElemento + '">' + mensagem + '</span>');
}

function mudaEscritaBotaoEntrando(mudar) {
    if (mudar)
        $("#Botao-Entrar").attr("class", "Botao-Login-Entrando").attr("value", "Entrando");
    else
        $("#Botao-Entrar").attr("class", "Botao-Login").attr("value", "Entrar");
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getReturnUrl() {
    return getUrlVars()["ReturnUrl"];
}

function loginViewModel() {
    var self = this;

    self.logar = function () {
        $('.input-validation-error').removeClass('input-validation-error');
        $('.field-validation-error').children().html('');
        var form = $('#FormularioLogin');
        if (form.valid()) {
            mudaEscritaBotaoEntrando(true);
            $.post(urlLogin, form.serialize(), function (retorno) {
                if (retorno.Logado) {
                    location.href = retorno.CaminhoRetorno;
                } else if (retorno.Login) {
                    mensagemValidacaoCampos('Login', retorno.Mensagem, form);
                    mudaEscritaBotaoEntrando(false);
                } else if (retorno.Senha) {
                    mensagemValidacaoCampos('Senha', retorno.Mensagem, form);
                    mudaEscritaBotaoEntrando(false);
                } else if (retorno.Modal) {
                    $("#Janela-Modal").modalAdvertencia(retorno.Mensagem, retorno.Solucao).dialog("open");
                    mudaEscritaBotaoEntrando(false);
                }
            }, "json");
        }
    };
}