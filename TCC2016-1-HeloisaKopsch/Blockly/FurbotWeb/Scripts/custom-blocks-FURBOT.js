﻿arrayDirecoes = [["DIREITA", "DIREITA"], ["ESQUERDA", "ESQUERDA"], ["ACIMA", "ACIMA"], ["ABAIXO", "ABAIXO"], ["AQUIMESMO", "AQUIMESMO"]];
arrayObjTipo = [["ALIEN", "ALIEN"], ["PAREDE", "PAREDE"], ["ALIENMOVEL", "ALIENMOVEL"], ["PROJETIL", "PROJETIL"], ["TESOURO", "TESOURO"]];

//INÍCIO COMANDOS MOVIMENTAÇÃO
Blockly.Blocks['furbot_andar_direita'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("andarDireita");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(330);
        this.setTooltip('faz o robô andar para a direita');
    }
};

Blockly.JavaScript['furbot_andar_direita'] = function (block) {
    var code = 'andarDireita(); \n';
    return code;
};

Blockly.Blocks['furbot_andar_esquerda'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("andarEsquerda");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(330);
        this.setTooltip('faz o robô andar para a esquerda');
    }
};

Blockly.JavaScript['furbot_andar_esquerda'] = function (block) {
    var code = 'andarEsquerda(); \n';
    return code;
};

Blockly.Blocks['furbot_andar_acima'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("andarAcima");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(330);
        this.setTooltip('faz o robô andar para cima');
    }
};

Blockly.JavaScript['furbot_andar_acima'] = function (block) {
    var code = 'andarAcima(); \n';
    return code;
};

Blockly.Blocks['furbot_andar_abaixo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("andarAbaixo");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(330);
        this.setTooltip('faz o robô andar para baixo');
    }
};

Blockly.JavaScript['furbot_andar_abaixo'] = function (block) {
    var code = 'andarAbaixo(); \n';
    return code;
};

Blockly.Blocks['furbot_mudar_posicao'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("mudarPosicao")
            .appendField(new Blockly.FieldTextInput("X"), "COORD_X")
            .appendField(new Blockly.FieldTextInput("Y"), "COORD_Y");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(330);
        this.setTooltip('muda posição do robô para a coordenada x,y');
    }
};

Blockly.JavaScript['furbot_mudar_posicao'] = function (block) {
    var text_obj_x = block.getFieldValue('COORD_X');
    var text_obj_y = block.getFieldValue('COORD_Y');
    var code = 'mudarPosicao("' + text_obj_x + '", "' + text_obj_y + '"); \n';
    return code;
};
//FIM COMANDOS MOVIMENTAÇÃO

//INÍCIO COMANDOS COMUNICAÇÃO
Blockly.Blocks['furbot_diga'] = {
    init: function () {
        this.appendValueInput("texto")
            .appendField("diga");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(300);
        this.setTooltip('faz o robô dizer uma palavra/frase');
    }
};

Blockly.JavaScript['furbot_diga'] = function (block) {
    var value_texto = Blockly.JavaScript.valueToCode(block, 'texto', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'diga(' + value_texto + '); \n';
    return code;
};
//FIM COMANDOS COMUNICAÇÃO

//INÍCIO COMANDOS AVALIAÇÃO DO MUNDO
Blockly.Blocks['furbot_eh_vazio'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("ehVazio")
            .appendField(new Blockly.FieldDropdown(arrayDirecoes), "DIRECAO");
        this.setOutput(true);
        this.setColour(270);
        this.setTooltip('verifica se direção indicada está vazia');
    }
};

Blockly.JavaScript['furbot_eh_vazio'] = function (block) {
    var dropdown_direcao = block.getFieldValue('DIRECAO');
    var code = 'ehVazio("' + dropdown_direcao + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_eh_fim'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("ehFim")
            .appendField(new Blockly.FieldDropdown(arrayDirecoes), "DIRECAO");
        this.setOutput(true);
        this.setColour(270);
        this.setTooltip('verifica se direção indicada é o fim do mundo do robô');
    }
};

Blockly.JavaScript['furbot_eh_fim'] = function (block) {
    var dropdown_direcao = block.getFieldValue('DIRECAO');
    var code = 'ehFim("' + dropdown_direcao + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};
//FIM COMANDOS AVALIAÇÃO DO MUNDO

//INÍCIO COMANDOS OBTENÇÃO DE COORDENADAS
Blockly.Blocks['furbot_get_x'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getX");
        this.setOutput(true);
        this.setColour(240);
        this.setTooltip('retorna o número da coluna em que está o robô');
    }
};

Blockly.JavaScript['furbot_get_x'] = function (block) {
    var code = 'getX()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};


Blockly.Blocks['furbot_get_y'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getY");
        this.setOutput(true);
        this.setColour(240);
        this.setTooltip('retorna o número da linha em que está o robô');
    }
};

Blockly.JavaScript['furbot_get_y'] = function (block) {
    var code = 'getY()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};
//FIM COMANDOS OBTENÇÃO DE COORDENADAS

//INÍCIO COMANDOS MANIPULAÇÃO DE OBJETOS
Blockly.Blocks['furbot_get_objeto'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getObjeto")
            .appendField(new Blockly.FieldDropdown(arrayDirecoes), "DIRECAO");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('o robô agarra o objeto na direção indicada');
    }
};

Blockly.JavaScript['furbot_get_objeto'] = function (block) {
    var dropdown_direcao = block.getFieldValue('DIRECAO');
    var code = 'getObjeto("' + dropdown_direcao + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_eh_obj_tipo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("ehObjetoDoMundoTipo")
            .appendField(new Blockly.FieldDropdown(arrayObjTipo), "OBJ_TIPO")
            .appendField(new Blockly.FieldDropdown(arrayDirecoes), "DIRECAO");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('verifica se existe um objeto do tipo informado na direção informada');
    }
};

Blockly.JavaScript['furbot_eh_obj_tipo'] = function (block) {
    var dropdown_obj_tipo = block.getFieldValue('OBJ_TIPO');
    var dropdown_direcao = block.getFieldValue('DIRECAO');
    var code = 'ehObjetoDoMundoTipo("' + dropdown_obj_tipo + '", "' + dropdown_direcao + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_add_obj_dir'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("adicionarObjetoNoMundo")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO")
            .appendField(new Blockly.FieldDropdown(arrayDirecoes), "DIRECAO");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(210);
        this.setTooltip('adiciona um objeto no mundo na posição relativa à direção informada');
    }
};

Blockly.JavaScript['furbot_add_obj_dir'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var dropdown_direcao = block.getFieldValue('DIRECAO');
    var code = 'adicionarObjetoNoMundo(' + variable_obj_mundo + ', "' + dropdown_direcao + '"); \n';
    return code;
};

Blockly.Blocks['furbot_add_obj_coor'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("adicionarObjetoNoMundoXY")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO")
            .appendField(new Blockly.FieldTextInput("X"), "COORD_X")
            .appendField(new Blockly.FieldTextInput("Y"), "COORD_Y");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(210);
        this.setTooltip('adiciona um objeto no mundo na coordenada x,y');
    }
};

Blockly.JavaScript['furbot_add_obj_coor'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var text_obj_x = block.getFieldValue('COORD_X');
    var text_obj_y = block.getFieldValue('COORD_Y');
    var code = 'adicionarObjetoNoMundoXY(' + variable_obj_mundo + ', "' + text_obj_x + '", "' + text_obj_y + '"); \n';
    return code;
};

Blockly.Blocks['furbot_remover_obj'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("removerObjetoDoMundo")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(210);
        this.setTooltip('remove um objeto do mundo passado como parâmetro');
    }
};

Blockly.JavaScript['furbot_remover_obj'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'removerObjetoDoMundo(' + variable_obj_mundo + '); \n';
    return code
};

Blockly.Blocks['furbot_get_objeto_coor'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getObjetoXY")
            .appendField(new Blockly.FieldTextInput("X"), "COORD_X")
            .appendField(new Blockly.FieldTextInput("Y"), "COORD_Y");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('recupera objeto na coordenada x,y');
    }
};

Blockly.JavaScript['furbot_get_objeto_coor'] = function (block) {
    var text_obj_x = block.getFieldValue('COORD_X');
    var text_obj_y = block.getFieldValue('COORD_Y');
    var code = 'getObjetoXY("' + text_obj_x + '", "' + text_obj_y + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_var_alien'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Alien");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('seta tipo da variável para Alien');
    }
};

Blockly.JavaScript['furbot_var_alien'] = function (block) {
    var code = 'Alien()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_var_parede'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Parede");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('seta tipo da variável para Parede');
    }
};

Blockly.JavaScript['furbot_var_parede'] = function (block) {
    var code = 'Parede()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_var_alien_movel'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("AlienMovel");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('seta tipo da variável para AlienMovel');
    }
};

Blockly.JavaScript['furbot_var_alien_movel'] = function (block) {
    var code = 'AlienMovel()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_var_projetil'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Projetil");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('seta tipo da variável para Projetil');
    }
};

Blockly.JavaScript['furbot_var_projetil'] = function (block) {
    var code = 'Projetil()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_var_tesouro'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Tesouro");
        this.setOutput(true);
        this.setColour(210);
        this.setTooltip('seta tipo da variável para Tesouro');
    }
};

Blockly.JavaScript['furbot_var_tesouro'] = function (block) {
    var code = 'Tesouro()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};
//FIM COMANDOS MANIPULAÇÃO DE OBJETOS

//INÍCIO OUTROS COMANDOS
Blockly.Blocks['furbot_eh_dependente_energia'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("ehDependenteEnergia");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('retorna true se o robô precisa de energia para executar os métodos');
    }
};

Blockly.JavaScript['furbot_eh_dependente_energia'] = function (block) {
    var code = 'ehDependenteEnergia()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_get_max_energia'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getMaxEnergia");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('retorna a quantidade máxima de energia que pode ser armazena no robô');
    }
};

Blockly.JavaScript['furbot_get_max_energia'] = function (block) {
    var code = 'getMaxEnergia()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_get_energia'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getEnergia");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('retorna a quantidade atual de energia');
    }
};

Blockly.JavaScript['furbot_get_energia'] = function (block) {
    var code = 'getEnergia()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_esperar'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("esperar")
            .appendField(new Blockly.FieldTextInput("segundos"), "SEG");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('o robô espera a quantidade de segundos passado como parâmetro antes de executar o próximo método');
    }
};

Blockly.JavaScript['furbot_esperar'] = function (block) {
    var text_segundos = block.getFieldValue('SEG');
    var code = 'esperar("' + text_segundos + '"); \n';
    return code;
};

Blockly.Blocks['furbot_esperar_alguem'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("esperarAlguem");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('O robô espera até algum objeto cruzar a mesma célula em que ele está, e retorna esse objeto. Caso se passem 5 segundos e nenhum objeto ter cruzada a célula, então retorna null');
    }
};

Blockly.JavaScript['furbot_esperar_alguem'] = function (block) {
    var code = 'esperarAlguem(); \n';
    return code;
};

Blockly.Blocks['furbot_sortear'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("sortear")
            .appendField(new Blockly.FieldTextInput("MAX"), "MAX");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('sorteia um valor entre 0 e o valor máximo passado como parâmetro');
    }
};

Blockly.JavaScript['furbot_get_objeto_coor'] = function (block) {
    var text_obj_x = block.getFieldValue('COORD_X');
    var text_obj_y = block.getFieldValue('COORD_Y');
    var code = 'getObjetoXY("' + text_obj_x + '", "' + text_obj_y + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['furbot_sortear'] = function (block) {
    var text_max = block.getFieldValue('MAX');
    var code = 'sortear("' + text_max + '")';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_jah_explodiu'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("jahExplodiu");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('retorna true se o robô já explodiu');
    }
};

Blockly.JavaScript['furbot_jah_explodiu'] = function (block) {
    var code = 'jahExplodiu()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_add_obj_embaixo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("adicionarObjetoNoMundoEmbaixo")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('adiciona objeto no mundo na posição embaixo da atual posição');
    }
};


Blockly.JavaScript['furbot_add_obj_embaixo'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'adicionarObjetoNoMundoEmbaixo(' + variable_obj_mundo + '); \n';
    return code;
};

Blockly.Blocks['furbot_eh_bloqueado'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("ehBloqueado")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('verifica se o objeto é bloqueado ou não');
    }
};

Blockly.JavaScript['furbot_eh_bloqueado'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'ehBloqueado(' + variable_obj_mundo + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['furbot_bloquear'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("bloquear")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('bloqueia um objeto');
    }
};

Blockly.JavaScript['furbot_bloquear'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'bloquear(' + variable_obj_mundo + '); \n';
    return code;
};

Blockly.Blocks['furbot_desbloquear'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("desbloquear")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('desbloqueia um objeto');
    }
};

Blockly.JavaScript['furbot_desbloquear'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'desbloquear(' + variable_obj_mundo + '); \n';
    return code;
};

Blockly.Blocks['furbot_get_sou_tipo'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("getSouDoTipo")
            .appendField(new Blockly.FieldVariable("selecione o objeto"), "OBJ_MUNDO");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('retorna o tipo do objeto');
    }
};

Blockly.JavaScript['furbot_get_sou_tipo'] = function (block) {
    var variable_obj_mundo = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('OBJ_MUNDO'), Blockly.Variables.NAME_TYPE);
    var code = 'getSouDoTipo(' + variable_obj_mundo + ')';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

//@Deprecated
//Blockly.Blocks['furbot_get_velocidade'] = {
//    init: function () {
//        this.appendDummyInput()
//            .appendField("getVelocidade");
//        this.setPreviousStatement(true);
//        this.setNextStatement(true);
//        this.setColour(180);
//        this.setTooltip('retorna a velocidade do robô');
//    }
//};

//Blockly.JavaScript['furbot_get_velocidade'] = function (block) {
//    var code = 'getVelocidade(); \n';
//    return code;
//};

//Blockly.Blocks['furbot_set_velocidade'] = {
//    init: function () {
//        this.appendDummyInput()
//            .appendField("setVelocidade")
//            .appendField(new Blockly.FieldTextInput("velocidade"), "VELOCIDADE");
//        this.setOutput(true);
//        this.setColour(150);
//        this.setTooltip('altera a velocidade do robô');
//    }
//};

//Blockly.JavaScript['furbot_set_velocidade'] = function (block) {
//    var text_velocidade = block.getFieldValue('VELOCIDADE');
//    var code = 'setVelocidade("' + text_velocidade + '")';
//    return [code, Blockly.JavaScript.ORDER_NONE];
//};
//FIM OUTROS COMANDO
