﻿function correcaoExercicioViewModel() {
    var self = this;
    ko.mapping.fromJS(jsonViewModel, {}, self);
    self.mostrarBotoesLinhaSelecionada = ko.observable(false);
    self.itemSelecionado = ko.observable();

    self.gridConfiguration = new ko.simpleGrid.viewModel({
        data: self.Resolucoes,
        columns: [
            { headerText: "Nome do aluno", rowText: "NomeAluno" },
            { headerText: "Título do exercício", rowText: "TituloExercicio" },
            { headerText: "Data da resolução", rowText: "DataResolucao" }
        ],
        pageSize: 5
    });

    self.consultar = function () {
        self.mostrarBotoesLinhaSelecionada(false);
        $.ajax({
            url: urlPadrao + 'FiltrarResolucoes',
            type: 'POST',
            data: ko.toJSON(self.Filtros),
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function (retorno) {
            ko.mapping.fromJS(retorno, {}, self.Resolucoes);
        });
    };

    self.corrigir = function () {
        location.href = urlPadrao + 'Correcao?id=' + self.itemSelecionado();
    };
}