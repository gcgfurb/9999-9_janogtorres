﻿//<reference path="jquery/datepicker-pt-BR.js" />

$(function () {
    $.datepicker.setDefaults($.datepicker.regional["pt-BR"]);
    $.ajaxSetup
    ({
        cache: false
    });
    ConfigurarValidacao();

    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (jqxhr.status == 401) {
            var url = jqxhr.getResponseHeader("Location");
            var loginUrl = url.split("?")[0].split("#")[0];

            var origin = window.location.origin || window.location.protocol + "//" + window.location.host;
            var loginWithReturnUrl = origin + loginUrl + "?ReturnUrl=" + encodeURIComponent(window.location.pathname);

            window.location.href = loginWithReturnUrl;
        }
        else if (jqxhr.status == 500) {
            window.location.href = window.location.origin + "/" + window.location.pathname.split("/")[1] + "/Erros/InternalServer";
        }
    });
});

function ConfigurarTratamentosCampos() {
    $('input[type=text]').focusout(function () {
        $(this).val($.trim($(this).val())).change();
    });
}

function ConfigurarValidacao(element) {
    if (!element)
        element = "";

    $.each($(element + ' form'), function (index, item) {
        //$.each($(element + ' form').find("input, textarea, select"), function (i, it) {
        //    var height = "26px";
        //    if ($(it).attr("data-required") != null) {
        //        if (!$(it).is("input:text") && !$(it).is("input:hidden") && !$(it).is("input:password") && !$(it).is("select") && $(it).attr("data-role") != "autocomplete") {
        //            height = $(it).css("height");
        //        }
        //        AdicionarTarjaCampoObrigatorio(it, height);                
        //    }
        //});

        var validator = $.data(item, 'validator');
        if (validator) {
            var settngs = validator.settings;
            settngs.ignore = [];
            highlight = settngs.highlight;
            unhighlight = settngs.unhighlight;
            settngs.onkeyup = false;
            settngs.onfocusout = false;
        }

    });
}