﻿var ClasseCSSMensagemModalBarraTitulo = "Mensagem-Modal-BarraTitulo";
var ClasseCSSMensagemModalTituloBranco = "Mensagem-Modal-TituloBranco";
var ClasseCSSMensagemModalErro = "Mensagem-Modal-Erro";
var ClasseCSSMensagemModalSucesso = "Mensagem-Modal-Sucesso";
var ClasseCSSMensagemModalInformacao = "Mensagem-Modal-Informacao";
var ClasseCSSMensagemModalAdvertencia = "Mensagem-Modal-Advertencia";
var ClasseCSSModalConteudo = "Modal-Conteudo";
var ClasseCSSModalBotaoFechar = "Modal-BotaoFechar";
var ClasseCSSModalIconeBotaoFechar = "Modal-BotaoFecharIcone";
var ClasseCSSModalTitulo = "Modal-Titulo";
var ClasseCSSModalBarraTitulo = "Modal-BarraTitulo";

var requestTimer = null;
$(document).ajaxStart(function () {
    requestTimer = setTimeout(function () {
        $("#modalLoadingPanel").show();
        $("input[type=button]").prop('disabled', true);
    }, 1000);
});

$(document).ajaxStop(function () {
    if (requestTimer != null) {
        clearTimeout(requestTimer);
    }

    $("input[type=button]").not('.ignorar').prop('disabled', false);
    $("#modalLoadingPanel").hide();
});

jQuery.fn.modalConfirmacao = function (mensagem, funcaoConfirmacao, funcaoNegacao, sobreModal) {
    return this.each(function () {
        var dialog = $(this);

        var htmlValue = "<div class='Alinhar-Centro'>" +
            "<label class='Mensagem-Dialog'>" + mensagem + " </label><br/> ";

        htmlValue +=
            "<button type='button' class='Botao Botao-Fechar-Modal'>" +
                "<div>Não</div>" +
                "</button>";

        htmlValue +=
            "<button type='button' class='Botao Botao-Sim-Modal'>" +
                "<div>Sim</div>" +
                "</button>";

        // Teste de resolução para mobile
        var _width = 800;
        if ($(document).width() <= 500)
            _width = 400;

        dialog.html(htmlValue);
        dialog.dialog({
            autoOpen: false,
            width: _width,
            height: 'auto',
            draggable: false,
            resizable: false,
            minHeight: 129,
            modal: true,
            title: 'Mensagem de Advertência'
        });

        $(".Botao-Fechar-Modal").click(
            function (e) {
                e.preventDefault();
                dialog.dialog("close");
                if (funcaoNegacao)
                    funcaoNegacao();
            });

        $(".Botao-Sim-Modal").click(
            function (e) {
                e.preventDefault();
                dialog.dialog("close");
                if (funcaoConfirmacao)
                    funcaoConfirmacao();
            });

        if (!sobreModal) {
            limparEstilosModal();

            $(".ui-dialog-titlebar").addClass(ClasseCSSMensagemModalBarraTitulo + " " + ClasseCSSMensagemModalAdvertencia);
            $(".ui-dialog-title").addClass(ClasseCSSMensagemModalTituloBranco);

            $(".ui-dialog-titlebar-close").hide();
        }

    });
};

jQuery.fn.modalConfirmacaoComDetalhes = function (mensagem, detalhes, funcaoConfirmacao, funcaoNegacao) {
    limparEstilosModal();
    return this.each(function () {
        var dialog = $(this);

        var htmlValue = "<div class='Alinhar-Centro'>" +
            "<label class='Mensagem-Dialog'>" + mensagem + " </label><br/> ";

        if (detalhes != null) {

            htmlValue +=
                "<a href='#' class='Link-Mostrar-Detalhes-Modal'>Mais detalhes</a>" +
                    "</div>" +
                    "<div class='Div-Detalhes-Modal'>" + detalhes + "</div>";
        }

        htmlValue +=
            "<button type='button' class='Botao Botao-Fechar-Modal'>" +
                "<div>Não</div>" +
                "</button>";

        htmlValue +=
            "<button type='button' class='Botao Botao-Sim-Modal'>" +
                "<div>Sim</div>" +
                "</button>";

        // Teste de resolução para mobile
        var _width = 800;
        if ($(document).width() <= 500)
            _width = 400;

        dialog.html(htmlValue);
        dialog.dialog({
            autoOpen: false,
            width: _width,
            height: 'auto',
            draggable: false,
            resizable: false,
            minHeight: 129,
            modal: true,
            title: 'Mensagem de Advertência'
        });

        $(".Botao-Fechar-Modal").click(
            function (e) {
                e.preventDefault();
                dialog.dialog("close");
                if (funcaoNegacao)
                    funcaoNegacao();
            });

        $(".Botao-Sim-Modal").click(
            function (e) {
                e.preventDefault();
                $('div#Janela-Modal').dialog("close");
                if (funcaoConfirmacao)
                    funcaoConfirmacao();
            });

        $(".ui-dialog-titlebar").addClass(ClasseCSSMensagemModalBarraTitulo + " " + ClasseCSSMensagemModalAdvertencia);
        $(".ui-dialog-title").addClass(ClasseCSSMensagemModalTituloBranco);

        $(".ui-dialog-titlebar-close").hide();

        $(".Link-Mostrar-Detalhes-Modal").click(function (e) {
            e.preventDefault();
            $(".Div-Detalhes-Modal").toggle();
            if ($(".Div-Detalhes-Modal").is(":visible")) {
                $(".Link-Mostrar-Detalhes-Modal").text('Menos detalhes');
            } else {
                $(".Link-Mostrar-Detalhes-Modal").text('Mais detalhes');
            }
        });
    });
};

jQuery.fn.modalErro = function (mensagem, detalhes) {
    return montarMensagemAviso(this, mensagem, detalhes, 'erro', ClasseCSSMensagemModalErro);
};

jQuery.fn.modalAdvertencia = function (mensagem, detalhes) {
    return montarMensagemAviso(this, mensagem, detalhes, 'advertência', ClasseCSSMensagemModalAdvertencia);
};

jQuery.fn.modalInformacao = function (mensagem, detalhes, urlRetorno) {
    return montarMensagemAviso(this, mensagem, detalhes, 'informação', ClasseCSSMensagemModalInformacao, urlRetorno);
};

jQuery.fn.modalSucesso = function (mensagem, detalhes, urlRetorno) {
    return montarMensagemAviso(this, mensagem, detalhes, 'sucesso', ClasseCSSMensagemModalSucesso, urlRetorno);
};

function limparEstilosModal() {

    $(".ui-dialog-title").removeClass(ClasseCSSModalTitulo + " " + ClasseCSSMensagemModalTituloBranco);

    $(".ui-dialog-titlebar").removeClass(ClasseCSSModalBarraTitulo + " " +
                                       ClasseCSSMensagemModalBarraTitulo + " " +
                                       ClasseCSSMensagemModalAdvertencia + " " +
                                       ClasseCSSMensagemModalErro + " " +
                                       ClasseCSSMensagemModalInformacao + " " +
                                       ClasseCSSMensagemModalSucesso);

    $(".ui-dialog .ui-dialog-titlebar-close").removeClass(ClasseCSSModalBotaoFechar);
    $(".ui-dialog .ui-dialog-titlebar-close span").removeClass(ClasseCSSModalIconeBotaoFechar);

    $(".ui-dialog-content").removeClass(ClasseCSSModalConteudo);
}

function montarMensagemAviso(item, mensagem, detalhes, tipoAviso, classeCSS, urlRetorno) {
    limparEstilosModal();

    return item.each(function () {
        var dialog = $(this);

        var htmlValue = "<div class='Alinhar-Centro'>" +
            "<label class='Mensagem-Dialog'>" + mensagem + " </label><br/> ";

        if (detalhes != null) {

            htmlValue +=
                "<a class='Link-Mostrar-Detalhes-Modal'>Mais detalhes</a>" +
                    "</div>" +
                    "<div class='Div-Detalhes-Modal'>" + detalhes + "</div>";
        }

        htmlValue +=
            "<button type='button' class='Botao Botao-Fechar-Modal'>" +
                "<div>OK</div>" +
                "</button>";

        // Teste de resolução para mobile
        var _width = 800;
        if ($(document).width() <= 500)
            _width = 400;

        dialog.html(htmlValue);
        dialog.dialog({
            autoOpen: false,
            width: _width,
            height: 'auto',
            resizable: false,
            minHeight: 129,
            draggable: false,
            modal: true,
            title: 'Mensagem de ' + tipoAviso
        });

        $(".Botao-Fechar-Modal").click(
            function (e) {
                e.preventDefault();
                dialog.dialog("close");
                if (urlRetorno != null && urlRetorno != "") {
                    location = urlRetorno;
                }
            });
        $(".ui-dialog-titlebar-close").hide();

        $(".ui-dialog-titlebar").addClass(ClasseCSSMensagemModalBarraTitulo + " " + classeCSS);
        $(".ui-dialog-title").addClass(ClasseCSSMensagemModalTituloBranco);


        $(".Link-Mostrar-Detalhes-Modal").click(function (e) {
            e.preventDefault();
            $(".Div-Detalhes-Modal").toggle();
            if ($(".Div-Detalhes-Modal").is(":visible")) {
                $(".Link-Mostrar-Detalhes-Modal").text('Menos detalhes');
            } else {
                $(".Link-Mostrar-Detalhes-Modal").text('Mais detalhes');
            }
        });
    });
}

jQuery.fn.modal = function (urlPagina, titulo, largura, altura, podeFechar, funcao, onClose) {
    limparEstilosModal();
    return this.each(function () {
        var dialog = $(this);
        dialog.load(urlPagina, function () {

            dialog.dialog({
                autoOpen: false,
                height: altura,
                width: largura,
                modal: true,
                closeOnEscape: podeFechar,
                resizable: false,
                draggable: false,
                title: titulo,
                close: function (event, ui) {
                    if (onClose != undefined && onClose != null)
                        onClose();
                },
                open: function () {
                    $('.Modal-Conteudo').on('click', '.k-widget.k-dropdown.k-header', function () {
                        $(".Modal-Conteudo input:focus").blur();
                    });
                }
            });
            if (!podeFechar)
                $(".ui-dialog-titlebar-close").hide();

            $(".ui-dialog-content").addClass(ClasseCSSModalConteudo);

            $(".ui-dialog .ui-dialog-titlebar-close").addClass(ClasseCSSModalBotaoFechar);
            $(".ui-dialog .ui-dialog-titlebar-close span").addClass(ClasseCSSModalIconeBotaoFechar);

            $(".ui-dialog-title").addClass(ClasseCSSModalTitulo);
            $(".ui-dialog-titlebar").addClass(ClasseCSSModalBarraTitulo);

            dialog.dialog("open");

            funcao();
        });
    });
};

function removeMensagem(elemento) {
    $(elemento).parent().remove();
}

function exibirErros(formulario, erros) {
    $.each(erros, function (index, item) {

        var elements = formulario.find('[name$="' + item.PropertyName + '"]');

        $.each(elements, function () {
            var elemento = $(this);
            if (elemento.attr("data-role") == "dropdownlist") {
                elemento.parent().find(".k-dropdown-wrap").addClass('input-validation-error');
                elemento.parent().find(".k-dropdown-wrap").show();
            }
            else
                elemento.addClass('input-validation-error');
        });

        if (elements.length) {
            var fieldContainer = $('#' + elements[0].id).parents('.Esconder');
            if (!fieldContainer.is(':visible')) {
                fieldContainer.parents(".Bloco-Separador").find(".Controle-Fieldset").trigger('click');
            }
        }

        componente = formulario.find('[data-valmsg-for$="' + item.PropertyName + '"]');
        componente.html('<span>' + item.Message + '</span>');
        componente.show();
        highlight(componente, "field-validation-error", "field-validation-valid");
    });
}

function obterListaErrosJQueryValidator(formulario) {
    var map = {};
    var elementos = formulario.find('.input-validation-error');
    if (elementos.length == 0) {
        formulario.find('.error').each(function () {
            var element = this;
            var txt = $('#' + element.id + '_validationMessage').text();
            if (txt == "") {
                txt = $(element).parent().find('label[for="' + element.id + '"]').html();
            }
            map[element.name] = { PropertyName: element.name, Message: txt };
        });
    }
    else {
        elementos.each(function () {
            var element = this;
            var txt = $('#' + element.id + '_validationMessage').text();
            if (txt == "") {
                txt = formulario.find('[data-valmsg-for="' + element.name + '"]').find('span').html();
            }
            map[element.name] = { PropertyName: element.name, Message: txt };
        });
    }

    return map;
}

String.prototype.endsWith = function (pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
};

function exibirFeedbackErroCampo(mensagemPrincipal, erros, elementoContainer) {
    var mensagem = mensagemPrincipal;
    mensagem += "<br><br>";
    $(elementoContainer).show();
    var inputs = [];
    $(elementoContainer).find(".input-validation-error").each(function () {
        if ($(this).is(":visible")) {
            if ($(this).is(":text"))
                inputs.push($(this));
            else
                inputs.push($(this).parent().find("input"));
        }
        var fieldContainer = $(this).parents('.Esconder');
        if (fieldContainer.css("display") == "none") {
            fieldContainer.parents(".Bloco-Separador").find(".Controle-Fieldset").trigger('click');
        }
    });

    var errosOrdenados = [];
    for (var i = 0; i < inputs.length; i++) {
        for (var j = 0; j < erros.length; j++) {
            var item = inputs[i];
            var erro = erros[j];
            var name = item.attr('name');

            if (name && name.endsWith(erro.PropertyName)) {
                errosOrdenados.push(erro);
                erros.splice(j, 1);
                break;
            }
        }
    }

    var errosParaMostrar = erros;

    if ($.isArray(erros))
        errosParaMostrar = erros.concat(errosOrdenados);

    $.each(errosParaMostrar, function (index, item) {
        if (index != 'undefined' && mensagem.indexOf(item.PropertyName) == -1) {
            var input = $(elementoContainer).find("[name$='" + item.PropertyName + "']");
            var label = null;
            if (input.attr('data-role') == 'datepicker')
                label = input.parent().parent().parent().parent().children("label").first();
            else if (input.attr('data-role') == 'dropdownlist' || input.attr('data-role') == 'autocomplete')
                label = input.parent().parent().parent().children("label").first();
            else
                label = input.parent().parent().children("label").first();

            mensagem += "&nbsp;&nbsp;&nbsp;&nbsp;";
            if (label.text())
                mensagem += "<b>" + label.text() + "</b>: ";

            mensagem += item.Message + "<br>";
        }
    });
    var html = '<div class="Aviso-Erro">';
    html += '   <span class="FechaAviso Erro">x</span>';
    html += '   <span>' + mensagem + '</span>';
    html += '</div>';
    var div = $(elementoContainer);
    if (div != null)
        div.find("div.Aviso-Erro").remove();


    div.prepend(html);

    div.find('div.Aviso-Erro').show('fade');
    $('html, body').animate({ scrollTop: div.offset().top - 100 }, 'slow');

}

function exibirFeedbackErro(mesagemPrincipal, elementoMostrar, erros) {
    var mensagem = mesagemPrincipal;
    if (erros != '' && erros != null) {
        mensagem += '<br /><br /> &nbsp; - ' + erros + '<br />';
    }

    var html = '<div class="Aviso-Erro">';
    html += '   <span class="FechaAviso Erro">x</span>';
    html += '   <span>' + mensagem + '</span>';
    html += '</div>';
    var div = $(elementoMostrar);
    if (div != null)
        div.find("div.Aviso-Erro").remove();
    $(elementoMostrar).prepend(html);
    $(elementoMostrar).find('div.Aviso-Erro').show('fade');

}

function exibirErrosNoModal(mesagemPrincipal, elementoMostrar, erros) {
    var mensagem = mesagemPrincipal + '<br /><br />';
    $.each(erros, function (index, item) {
        mensagem += '&nbsp; - ' + item + '<br />';
    });

    if (elementoMostrar == '') {
        elementoMostrar = 'span#erros';
    }

    var html = '<div class="Aviso-Erro">';
    html += '   <span class="FechaAviso Erro">x</span>';
    html += '   <span>' + mensagem + '</span>';
    html += '</div>';

    var div = $(elementoMostrar);
    if (div != null)
        div.find("div.Aviso-Erro").remove();

    $(elementoMostrar).prepend(html);
    $(elementoMostrar).find('div.Aviso-Erro').show('fade');
}

function exibirFeedbackAdvertencia(mensagem, elementoContainer) {
    var html = '<div class="Aviso-Advertencia">';
    html += '   <span class="FechaAviso Advertencia">x</span>';
    html += '   <span>' + mensagem + '</span>';
    html += '</div>';
    var div = $(elementoContainer);
    if (div != null)
        div.find("div.Aviso-Advertencia").remove();
    $(elementoContainer).prepend(html);
    $(elementoContainer).find('div.Aviso-Advertencia').show('fade');
}

function exibirFeedbackInformacao(informacoes, elementoContainer) {
    var html = '<div class="Aviso-Informacao">';
    html += '   <span class="FechaAviso Informacao">x</span>';
    html += '   <span>' + informacoes + '</span>';
    html += '</div>';
    var div = $(elementoContainer);
    if (div != null)
        div.find("div.Aviso-Informacao").remove();
    $(elementoContainer).prepend(html);
    $(elementoContainer).find('div.Aviso-Informacao').show('fade');
}

function esconderFeedbackInformacao(elementoContainer) {
    $(elementoContainer).find('div.Aviso-Informacao').remove();
}

function esconderFeedbackSucesso(elementoContainer) {
    $(elementoContainer).find('div.Aviso-Sucesso').remove();
}

function esconderFeedbackAdvertencia(elementoContainer) {
    $(elementoContainer).find('div.Aviso-Advertencia').remove();
}

function esconderFeedbackErro(elementoContainer) {
    $(elementoContainer).find('div.Aviso-Erro').remove();
}

function esconderTodosFeedbacks(elementoContainer) {
    esconderFeedbackInformacao(elementoContainer);
    esconderFeedbackSucesso(elementoContainer);
    esconderFeedbackAdvertencia(elementoContainer);
    esconderFeedbackErro(elementoContainer);
}

function exibirFeedbackSucesso(mensagem, elementoContainer) {
    var html = '<div class="Aviso-Sucesso">';
    html += '   <span class="FechaAviso Sucesso">x</span>';
    html += '   <span>' + mensagem + '</span>';
    html += '</div>';
    var div = $(elementoContainer);
    if (div != null)
        div.find("div.Aviso-Sucesso").remove();
    $(elementoContainer).prepend(html);
    $(elementoContainer).find('div.Aviso-Sucesso').show('fade');
    setTimeout(function () {
        $('div.Aviso-Sucesso').slideUp('slow', function () {
            $(this).remove();
        });
    }, 10000);
}

function exibirMensagemValidacao(formulario, nomeCampo, mensagem) {
    var arrayErros = [];
    var erro = new Object();
    erro.Message = mensagem;
    erro.PropertyName = nomeCampo;
    arrayErros.push(erro);

    exibirErros(formulario, arrayErros);
    $('div.Aviso-Erro').show('fade');
}

function removerMensagemValidacao(formulario, nomeCampo) {
    var elemento = $("[name$='" + nomeCampo + "']");
    if (elemento.attr("data-role") == "dropdownlist") {
        elemento.parent().find(".k-dropdown-wrap").removeClass('input-validation-error');
        elemento.parent().find(".k-dropdown-wrap").show();
    }
    else
        formulario.find('[name$="' + nomeCampo + '"]').removeClass('input-validation-error');

    $("[data-valmsg-for='" + nomeCampo + "']").html("");
}

function removerTodasMensagensValidacao() {
    $('.input-validation-error').removeClass('input-validation-error');
    $('.field-validation-error').children().html('');
}

function exibirMensagemModal(erro) {
    if (erro.Type == "Error")
        $('div#Janela-Modal').modalErro(erro.Message, erro.Solution).dialog('open');
    else
        $('div#Janela-Modal').modalAdvertencia(erro.Message, erro.Solution).dialog('open');
}

function exibirMensagemModalErro(erro, solution) {
    $('div#Janela-Modal').modalErro(erro, solution).dialog('open');
}

function exibirMensagemModalAdvertencia(erro, solution) {
    $('div#Janela-Modal').modalAdvertencia(erro, solution).dialog('open');
}

function aplicarMascaraDocumento() {
    $("input[data-type=doc]").setMask({ mask: '99999999999', autoTab: false });
}

function aplicarMascaraInteiro() {
    $("input[data-type=int]").setMask({ mask: '999999999999999', autoTab: false });
}

function aplicarMascaraDecimal() {
    $("input[data-type=decimal]").setMask({ mask: '99,99', autoTab: false });
}

function aplicarMascaraData() {
    $('input[data-type=date]').setMask({ mask: '39/19/9999', autoTab: false });
}

function scrollParaOTopo() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
}

function arrayObjectIndexOf(array, searchTerm, property) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][property] === searchTerm) return i;
    }
    return -1;
}

$(function () {
    $('div#Janela-Modal, div#Janela-Modal-Endereco, div#Janela-Modal-SelecaoEndereco, .Corpo').on("click", ".FechaAviso", function () { removeMensagem(this); });

    $('div#Janela-Modal, div#Janela-Modal-Endereco, .Corpo').on('click', '.Controle-Fieldset', function (e) {
        e.preventDefault();
        var pai = $(this).parent();
        var bloco = pai.find(".Esconder");
        if (bloco.css("display") == "none") {
            $(pai).find('div').removeClass('Seta-Fechado');
            $(pai).find('div').addClass('Seta-Aberto');
            bloco.show();
        } else {
            $(pai).find('div').removeClass('Seta-Aberto');
            $(pai).find('div').addClass('Seta-Fechado');
            bloco.hide();
        }
    });
});