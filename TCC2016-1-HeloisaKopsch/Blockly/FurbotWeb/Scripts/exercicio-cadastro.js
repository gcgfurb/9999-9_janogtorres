﻿function cadastroExercicioViewModel() {
    var self = this;
    ko.mapping.fromJS(jsonViewModel, {}, self);

    self.cadastrar = function () {
        $.ajax({
            url: urlPadrao + 'CadastrarExercicio',
            type: 'POST',
            data: ko.toJSON(self),
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function (retorno) {
            alert(retorno);
        });
    }
}