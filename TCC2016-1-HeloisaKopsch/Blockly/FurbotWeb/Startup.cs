﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FurbotWeb.Startup))]
namespace FurbotWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
