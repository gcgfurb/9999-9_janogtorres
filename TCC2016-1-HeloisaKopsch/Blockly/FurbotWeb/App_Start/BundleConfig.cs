﻿using System.Web;
using System.Web.Optimization;

namespace FurbotWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-{version}.js",
                        "~/Scripts/jquery/plugins.js",
                        "~/Scripts/jquery/menu.js"));

            bundles.Add(new ScriptBundle("~/bundles/mask").Include(
                        "~/Scripts/jquery/jquery.maskedinput-{version}.js",
                        "~/Scripts/jquery/jquery.meio.mask-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery/jquery-ui-{version}.js",
                        "~/Scripts/jquery/datepicker-pt-BR.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout/knockout-{version}.js",
                        "~/Scripts/knockout/knockout.mapping-latest.js",
                        "~/Scripts/knockout/knockout.simpleGrid-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/tecedufurb_engine")
                .Include(
               "~/Scripts/tecedufurb_engine/prototype-1.6.0.2.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/common/b2Settings.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/common/math/b2Vec2.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/common/math/b2Mat22.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/common/math/b2Math.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2AABB.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Bound.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2BoundValues.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Pair.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2PairCallback.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2BufferedPair.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2PairManager.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2BroadPhase.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Collision.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/Features.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2ContactID.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2ContactPoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Distance.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Manifold.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2OBB.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/b2Proxy.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/ClipVertex.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2Shape.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2ShapeDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2BoxDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2CircleDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2CircleShape.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2MassData.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2PolyDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/collision/shapes/b2PolyShape.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2Body.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2BodyDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2CollisionFilter.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2Island.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2TimeStep.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2ContactNode.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2Contact.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2ContactConstraint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2ContactConstraintPoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2ContactRegister.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2ContactSolver.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2CircleContact.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2Conservative.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2NullContact.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2PolyAndCircleContact.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/contacts/b2PolyContact.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2ContactManager.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2World.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/b2WorldListener.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2JointNode.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2Joint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2JointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2DistanceJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2DistanceJointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2Jacobian.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2GearJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2GearJointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2MouseJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2MouseJointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2PrismaticJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2PrismaticJointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2PulleyJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2PulleyJointDef.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2RevoluteJoint.js",
               "~/Scripts/tecedufurb_engine/physic/box2d/dynamics/joints/b2RevoluteJointDef.js",
               "~/Scripts/tecedufurb_engine/utils/ArrayUtils.js",
               "~/Scripts/tecedufurb_engine/utils/ComponentUtils.js",
               "~/Scripts/tecedufurb_engine/utils/JSUtils.js",
               "~/Scripts/tecedufurb_engine/utils/StringUtils.js",
               "~/Scripts/tecedufurb_engine/utils/ColorUtils.js",
               "~/Scripts/tecedufurb_engine/utils/MathUtils.js",
               "~/Scripts/tecedufurb_engine/collide/CollideInfo.js",
               "~/Scripts/tecedufurb_engine/component/Component.js",
               "~/Scripts/tecedufurb_engine/component/TransformationComponent.js",
               "~/Scripts/tecedufurb_engine/component/TranslateComponent.js",
               "~/Scripts/tecedufurb_engine/component/RenderableComponent.js",
               "~/Scripts/tecedufurb_engine/component/BoxRenderComponent.js",
               "~/Scripts/tecedufurb_engine/component/CircleRenderComponent.js",
               "~/Scripts/tecedufurb_engine/component/ImageRenderComponent.js",
               "~/Scripts/tecedufurb_engine/component/PolygonRenderComponent.js",
               "~/Scripts/tecedufurb_engine/component/RigidBodyComponent.js",
               "~/Scripts/tecedufurb_engine/component/RotateComponent.js",
               "~/Scripts/tecedufurb_engine/component/ScaleComponent.js",
               "~/Scripts/tecedufurb_engine/component/TranslateComponent.js",
               "~/Scripts/tecedufurb_engine/component/MoveCameraComponent.js",
               "~/Scripts/tecedufurb_engine/component/CameraComponent.js",
               "~/Scripts/tecedufurb_engine/component/FpsMeterComponent.js",
               "~/Scripts/tecedufurb_engine/component/AnimationRenderComponent.js",
               "~/Scripts/tecedufurb_engine/component/canvas/CanvasComponentFactory.js",
               "~/Scripts/tecedufurb_engine/component/threeJS/ThreeJSComponentFactory.js",
               "~/Scripts/tecedufurb_engine/component/canvas/CanvasTranslateComponent.js",
               "~/Scripts/tecedufurb_engine/component/canvas/CanvasScaleComponent.js",
               "~/Scripts/tecedufurb_engine/component/canvas/CanvasRotateComponent.js",
               "~/Scripts/tecedufurb_engine/component/threeJS/ThreeJSTranslateComponent.js",
               "~/Scripts/tecedufurb_engine/component/threeJS/ThreeJSScaleComponent.js",
               "~/Scripts/tecedufurb_engine/component/threeJS/ThreeJSRotateComponent.js",
               "~/Scripts/tecedufurb_engine/component/DragControlComponent.js",
               "~/Scripts/tecedufurb_engine/component/DraggableComponent.js",
               "~/Scripts/tecedufurb_engine/game/Asset.js",
               "~/Scripts/tecedufurb_engine/game/AssetStore.js",
               "~/Scripts/tecedufurb_engine/game/Camera.js",
               "~/Scripts/tecedufurb_engine/game/Game.js",
               "~/Scripts/tecedufurb_engine/game/Layer.js",
               "~/Scripts/tecedufurb_engine/game/Scene.js",
               "~/Scripts/tecedufurb_engine/geometric/Point.js",
               "~/Scripts/tecedufurb_engine/geometric/Point2D.js",
               "~/Scripts/tecedufurb_engine/geometric/Point3D.js",
               "~/Scripts/tecedufurb_engine/gameobject/GameObject.js",
               "~/Scripts/tecedufurb_engine/gameobject/canvas/CanvasObjectFactory.js",
               "~/Scripts/tecedufurb_engine/gameobject/threeJS/ThreeJSObjectFactory.js",
               "~/Scripts/tecedufurb_engine/gameobject/BoxObject.js",
               "~/Scripts/tecedufurb_engine/gameobject/CircleObject.js",
               "~/Scripts/tecedufurb_engine/gameobject/PolygonObject.js",
               "~/Scripts/tecedufurb_engine/system/KeySystem.js",
               "~/Scripts/tecedufurb_engine/system/LogicSystem.js",
               "~/Scripts/tecedufurb_engine/system/MouseSystem.js",
               "~/Scripts/tecedufurb_engine/system/RenderSystem.js",
               "~/Scripts/tecedufurb_engine/system/GamepadSystem.js",
               "~/Scripts/tecedufurb_engine/system/TouchSystem.js",
               "~/Scripts/tecedufurb_engine/api/APIHandler.js",
               "~/Scripts/tecedufurb_engine/api/HTML5CanvasHandler.js",
               "~/Scripts/tecedufurb_engine/api/ThreeJSHandler.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/Estilo.css"));
        }
    }
}
