/**
 * Created by Gabriel on 16/11/2015.
 */

function ScoreComponent(){}

ScoreComponent.prototype = new Component();

ScoreComponent.prototype.scores = 0;
ScoreComponent.prototype.scoreWins = 0;
ScoreComponent.prototype.scoreLoses = 0;

JSUtils.addMethod(ScoreComponent.prototype, "initialize",
    function(scoreWins, scoreLoses){
        this.initialize();
        this.scoreWins = scoreWins;
        this.scoreLoses = scoreLoses;
        return this;
    }
);

ScoreComponent.prototype.onWin = function(){
    this.scores += this.scoreWins;
};

ScoreComponent.prototype.onLose = function(){
    this.scores = (this.scores -= this.scoreLoses) > 0 ? this.scores : 0;
};

ScoreComponent.prototype.onRender = function(context){
    context.fillStyle = "red";
    context.font = "bold 20px Arial";
    context.fillText("Pontos: "+this.scores, 20, 30);
};

ScoreComponent.prototype.getSystems = function(){
    var systems = new Array();
    systems = ArrayUtils.addElement(systems, RenderSystem);
    return systems;
};

ScoreComponent.prototype.getTag = function(){
    return "SCORE_COMPONENT";
};