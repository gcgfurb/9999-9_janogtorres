﻿function consultaExercicioViewModel() {
    var self = this;
    ko.mapping.fromJS(jsonViewModel, {}, self);
    self.mostrarBotoesLinhaSelecionada = ko.observable(false);
    self.itemSelecionado = ko.observable();

    self.gridConfiguration = new ko.simpleGrid.viewModel({
        data: self.Resolucoes,
        columns: [
            { headerText: "Título do exercício", rowText: "TituloExercicio" },
            { headerText: "Data da resolução", rowText: "DataResolucao" },
            { headerText: "Nota", rowText: "Nota" }
        ],
        pageSize: 5
    });

    self.visualizar = function () {
        location.href = urlPadrao + 'Visualizar?id=' + self.itemSelecionado();
    };
}