﻿$(document).ready(function () {
    $('.Menu > ul > li').hover(
        function () {
            $(this).children('ul:first').stop(true, true).slideDown('fast');
        },
		function () {
		    $(this).children('ul:first').stop(true, true).slideUp('fast');
		}
    );

    $('.Usuario').hover(
        function () {
            $(this).children('ul').stop(true, true).slideDown('fast');
        },
		function () {
		    $(this).children('ul').stop(true, true).slideUp('fast');
		}
    );
});