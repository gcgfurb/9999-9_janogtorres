﻿function consultaExercicioViewModel() {
    var self = this;
    ko.mapping.fromJS(jsonViewModel, {}, self);
    self.mostrarBotoesLinhaSelecionada = ko.observable(false);
    self.itemSelecionado = ko.observable();

    self.gridConfiguration = new ko.simpleGrid.viewModel({
        data: self.Exercicios,
        columns: [
            { headerText: "Título", rowText: "Titulo" },
            { headerText: "Dificuldade", rowText: "Dificuldade" },
        ],
        pageSize: 5
    });

    self.consultar = function () {
        self.mostrarBotoesLinhaSelecionada(false);
        $.ajax({
            url: urlPadrao + 'FiltrarExercicios',
            type: 'POST',
            data: ko.toJSON(self.Filtros),
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function (retorno) {
            ko.mapping.fromJS(retorno, {}, self.Exercicios);
        });
    };

    self.alterar = function () {
        location.href = urlPadrao + 'Exercicio?id=' + self.itemSelecionado();
    };

    self.excluir = function () {
        self.mostrarBotoesLinhaSelecionada(false);
        $.ajax({
            url: urlPadrao + 'Excluir?id=' + self.itemSelecionado(),
            type: 'POST',
            cache: false,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(function (retorno) {
            if (retorno) {
                alert("Exercício excluído com sucesso!");
                self.consultar();
            }
        });
    };


}