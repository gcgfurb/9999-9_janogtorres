function RendererPiece(block) {
	this.properties = {};
	this._setupProperties();
	this.properties['mode'] = block.getFieldValue("mode");
	this.properties['clear'] = parseInt(block.getFieldValue("clear").replace("#", "0x"));
	this.properties['background'] = parseInt(block.getFieldValue("background").replace("#", "0x"));
	this.properties['axis'] = block.getFieldValue('axis') == 'TRUE';
	this.properties['grid'] = block.getFieldValue('grid') == 'TRUE';
	
}

RendererPiece.prototype = new Piece();
RendererPiece.prototype.constructor = RendererPiece;
RendererPiece.prototype.$setupProperties = RendererPiece.prototype._setupProperties;
RendererPiece.prototype.$load = RendererPiece.prototype.load;

RendererPiece.prototype.type = Types.typeRenderer;


RendererPiece.prototype._setupProperties = function() {
	this.properties['name'] = this.type.name;
	this.properties['clear'] = 0x101010;
	this.properties['background'] = 0x000000;
	this.properties['grid'] = true;
	this.properties['axis'] = true;
	this.properties['mode'] = '3D';
	this.update();
}

Blockly.Blocks['rendererpiece'] = {
		  init: function() {
		    this.appendDummyInput()
		        .appendField("Cor de Limpeza")
		        .appendField(new Blockly.FieldColour("#333333"), "clear");
		    this.appendDummyInput()
		        .appendField("Cor de Fundo")
		        .appendField(new Blockly.FieldColour("#000000"), "background");
		    this.appendDummyInput()
		        .appendField("Grade")
		        .appendField(new Blockly.FieldCheckbox("TRUE"), "grid");
		    this.appendDummyInput()
		        .appendField("Eixo")
		        .appendField(new Blockly.FieldCheckbox("TRUE"), "axis");
		    this.appendDummyInput()
		        .appendField("Gráficos")
		        .appendField(new Blockly.FieldDropdown([["3D", "3D"], ["2D", "2D"]]), "mode");
		    this.appendStatementInput("objetosgraficos")
	        	.setCheck("objetografico")
	        	.appendField("Gráficos");
		    this.setInputsInline(false);
		    this.setColour(230);
		    this.setTooltip('');
		    this.setHelpUrl('http://www.example.com/');
		  }
		};



RendererPiece.prototype.load = function(properties) {
	this.$load(properties);
	this.update();
	return this;
}
RendererPiece.prototype.update = function() {
	VisEdu.reloadProperties(this.properties);
}

RendererPiece.prototype.genGameObject = function() {
	return VisEdu.root;
}
