function GraphicObjectPiece() {}

GraphicObjectPiece.prototype = new Piece();
GraphicObjectPiece.prototype.constructor = GraphicObjectPiece;

GraphicObjectPiece.prototype.type = Types.typeGraphicObject;

GraphicObjectPiece.prototype.genGameObject = function() {
	this.properties = {'active':true};
	this._setupProperties();
	return VisEdu.factory.createGraphicObject(this);
}

GraphicObjectPiece.prototype.createObject = function(block) {
	this.properties = {'active':true};
	this._setupProperties();
	return this.genGameObject();
}

Blockly.Blocks['objetografico'] = {
		  init: function() {
		    this.appendDummyInput()
		        .appendField("Nome")
		        .appendField(new Blockly.FieldTextInput(""), "nome");
		    this.appendDummyInput()
		        .appendField("Ativo")
		        .appendField(new Blockly.FieldCheckbox("TRUE"), "ativo");
		    this.appendStatementInput("pecas")
		        .setCheck("cubo")
		        .appendField("Peças");
		    this.appendStatementInput("efeitos")
		        .setCheck("efeito")
		        .appendField("Efeitos");
		    this.appendValueInput("iluminacao")
		        .setCheck("iluminacao")
		        .appendField("Iluminação");
		    this.setInputsInline(false);
		    this.setPreviousStatement(true, null);
		    this.setTooltip('');
		    this.setHelpUrl('http://www.example.com/');
		  }
		};

Blockly.JavaScript['objetografico'] = function(block) {
	  var text_nome = block.getFieldValue('nome');
	  var checkbox_ativo = block.getFieldValue('ativo') == 'TRUE';
	  var statements_pecas = Blockly.JavaScript.statementToCode(block, 'pecas');
	  var statements_efeitos = Blockly.JavaScript.statementToCode(block, 'efeitos');
	  var value_iluminacao = Blockly.JavaScript.valueToCode(block, 'iluminacao', Blockly.JavaScript.ORDER_ATOMIC);
	  var code = '...;\n';
	  return code;
	};
