function CubePiece(block) {
	this.properties = {};
	this._setupProperties();
	this.properties['width'] = block.getFieldValue("tamanho_x");
	this.properties['height'] = block.getFieldValue("tamanho_y");
	this.properties['depth'] = block.getFieldValue("tamanho_z");
	this.properties['x'] = block.getFieldValue("posicao_x");
	this.properties['y'] = block.getFieldValue("posicao_y");
	this.properties['z'] = block.getFieldValue("posicao_z");
	this.properties['color'] = parseInt(block.getFieldValue("cor").replace("#", "0x"));
	this.properties['active'] = block.getFieldValue('ativo') == 'TRUE';;
}

CubePiece.prototype = new Piece();
CubePiece.prototype.constructor = CubePiece;
CubePiece.prototype.$_setupProperties = CubePiece.prototype._setupProperties;

CubePiece.prototype.type = Types.typeCube;

CubePiece.prototype.genGameObject = function() {
	return VisEdu.factory.createCube(this);
}

CubePiece.prototype._setupProperties = function() {
	this.$_setupProperties();
	this.properties['width'] = 100;
	this.properties['height'] = 100;
	this.properties['depth'] = 100;
	this.properties['x'] = 0;
	this.properties['y'] = 0;
	this.properties['z'] = 0;
	this.properties['color'] = 0xB4B4E1;
	this.properties['enableTexture'] = false;
	this.properties['textureImage'] = '';
	this.properties['listTextureImage'] = '';
}

Blockly.Blocks['cubo'] = {
		  init: function() {
		    this.appendDummyInput()
		        .appendField("Nome")
		        .appendField(new Blockly.FieldTextInput(""), "nome");
		    this.appendDummyInput()
		        .appendField("Ativo")
		        .appendField(new Blockly.FieldCheckbox("TRUE"), "ativo");
		    this.appendDummyInput()
		        .appendField("Cor")
		        .appendField(new Blockly.FieldColour("#ff0000"), "cor");
		    this.appendDummyInput()
		        .appendField("Posição");
		    this.appendDummyInput()
		        .appendField("X")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_x")
		        .appendField("Y")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_y")
		        .appendField("Z")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_z");
		    this.appendDummyInput()
		        .appendField("Tamanho");
		    this.appendDummyInput()
		        .appendField("X")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_x")
		        .appendField("Y")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_y")
		        .appendField("Z")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_z");
		    this.setInputsInline(false);
		    this.setPreviousStatement(true, null);
		    this.setColour(135);
		    this.setTooltip('teste');
		    this.setHelpUrl('http://www.example.com/');
		  },

		mutationToDom: function() {
			  var container = document.createElement('mutation');
			  var divisorInput = (this.getFieldValue('PROPERTY') == 'DIVISIBLE_BY');
			  container.setAttribute('divisor_input', divisorInput);
			  return container;
			},

		domToMutation: function(xmlElement) {
					var hasDivisorInput = (xmlElement.getAttribute('divisor_input') == 'true');
					//this.updateShape_(hasDivisorInput);  // Helper function for adding/removing 2nd input.
			},
			compose: function(topBlock) {
				  console.log(topBlock);
				},
				
				decompose: function(workspace) {
					  var topBlock = Blockly.Block.obtain(workspace, '');
					  topBlock.initSvg();
					  return topBlock;
					}
		};

Blockly.JavaScript['cubo'] = function(block) {
	  var text_nome = block.getFieldValue('nome');
	  var checkbox_ativo = block.getFieldValue('ativo') == 'TRUE';
	  var colour_cor = block.getFieldValue('cor');
	  var text_posicao_x = block.getFieldValue('posicao_x');
	  var text_posicao_y = block.getFieldValue('posicao_y');
	  var text_posicao_z = block.getFieldValue('posicao_z');
	  var text_tamanho_x = block.getFieldValue('tamanho_x');
	  var text_tamanho_y = block.getFieldValue('tamanho_y');
	  var text_tamanho_z = block.getFieldValue('tamanho_z');
	  var code = '...;\n';
	  return code;
	};
Blockly.Blocks['controls_cubo'] = {
		  /**
		   * Mutator bolck for else-if condition.
		   * @this Blockly.Block
		   */
		  init: function() {
			    this.appendDummyInput()
		        .appendField("Nome")
		        .appendField(new Blockly.FieldTextInput(""), "nome");
		    this.appendDummyInput()
		        .appendField("Ativo")
		        .appendField(new Blockly.FieldCheckbox("TRUE"), "ativo");
		    this.appendDummyInput()
		        .appendField("Cor")
		        .appendField(new Blockly.FieldColour("#ff0000"), "cor");
		    this.appendDummyInput()
		        .appendField("Posição");
		    this.appendDummyInput()
		        .appendField("X")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_x")
		        .appendField("Y")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_y")
		        .appendField("Z")
		        .appendField(new Blockly.FieldTextInput("0"), "posicao_z");
		    this.appendDummyInput()
		        .appendField("Tamanho");
		    this.appendDummyInput()
		        .appendField("X")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_x")
		        .appendField("Y")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_y")
		        .appendField("Z")
		        .appendField(new Blockly.FieldTextInput("100"), "tamanho_z");
		    this.setInputsInline(false);
		    this.setPreviousStatement(true, null);
		    this.setColour(135);
		    this.setTooltip('teste');
		    this.setHelpUrl('http://www.example.com/');
		    this.contextMenu = false;
		  }
		};	