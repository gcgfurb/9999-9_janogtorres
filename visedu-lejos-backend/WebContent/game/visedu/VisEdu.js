var toolbox = '<xml id="toolbox">';
toolbox += '<category" name="Comandos FURBOT" colour="360" expanded="false">';
toolbox += '		<categoryname="Movimentação" colour="330">';
toolbox += '			<block type="furbot_andar_direita"></block>';
toolbox += '			<block type="furbot_andar_esquerda"></block>';
toolbox += '			<block type="furbot_andar_acima"></block>';
toolbox += '			<block type="furbot_andar_abaixo"></block>';
toolbox += '			<block type="furbot_mudar_posicao"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Comunicação" colour="300">';
toolbox += '			<block type="furbot_diga"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Avaliação do mundo" colour="270">';
toolbox += '			<block type="furbot_eh_vazio"></block>';
toolbox += '			<block type="furbot_eh_fim"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Obtenção de coordenadas" colour="240">';
toolbox += '			<block type="furbot_get_x"></block>';
toolbox += '			<block type="furbot_get_y"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Manipulação de objetos" colour="210">';
toolbox += '			<block type="furbot_get_objeto"></block>';
toolbox += '			<block type="furbot_eh_obj_tipo"></block>';
toolbox += '			<block type="furbot_add_obj_dir"></block>';
toolbox += '			<block type="furbot_add_obj_coor"></block>';
toolbox += '			<block type="furbot_remover_obj"></block>';
toolbox += '			<block type="furbot_var_alien"></block>';
toolbox += '			<block type="furbot_var_parede"></block>';
toolbox += '			<block type="furbot_var_numero"></block>';
toolbox += '			<block type="furbot_get_objeto_coor"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Outros" colour="180">';
toolbox += '			<block type="furbot_eh_dependente_energia"></block>';
toolbox += '			<block type="furbot_get_max_energia"></block>';
toolbox += '			<block type="furbot_get_energia"></block>';
toolbox += '			<block type="furbot_esperar"></block>';
toolbox += '			<block type="furbot_esperar_alguem"></block>';
toolbox += '			<block type="furbot_sortear"></block>';
toolbox += '			<block type="furbot_jah_explodiu"></block>';
toolbox += '			<block type="furbot_add_obj_embaixo"></block>';
toolbox += '			<block type="furbot_eh_bloqueado"></block>';
toolbox += '			<block type="furbot_bloquear"></block>';
toolbox += '			<block type="furbot_desbloquear"></block>';
toolbox += '			<block type="furbot_get_sou_tipo"></block>';
toolbox += '		</category>';
toolbox += '	</category>';
toolbox += '	<category name="Comandos Básicos" expanded="false" colour="150">';
toolbox += '		<category name="Laço" colour="120">';
toolbox += '			<block type="controls_whileUntil"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Texto" colour="150">';
toolbox += '			<block type="text"></block>';
toolbox += '			<block type="text_join"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Lógica" colour="210">';
toolbox += '			<block type="controls_if"></block>';
toolbox += '			<block type="logic_negate"></block>';
toolbox += '			<block type="logic_compare"></block>';
toolbox += '			<block type="logic_operation"></block>';
toolbox += '			<block type="logic_boolean"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Matemática" colour="230">';
toolbox += '			<block type="math_number"></block>';
toolbox += '			<block type="math_arithmetic"></block>';
toolbox += '			<block type="math_number_property"></block>';
toolbox += '		</category>';
toolbox += '		<category name="Variáveis" colour="330">';
toolbox += '			<block type="variables_set" colour="330"></block>';
toolbox += '			<block type="variables_get"></block>';
toolbox += '		</category>';
toolbox += '	</category>';
toolbox += '	<category name="Comandos Básicos" expanded="false" colour="150">';
toolbox += '		<block type="objetografico"></block>';
toolbox += '		<block type="cubo"></block>';
toolbox += '	</category>';
toolbox += '</xml>';

var VisEdu = new function() {
	
	this.root = null;
	this.factory = null;
	this.factory3D = new Factory3D();
	this.factory2D = new Factory2D();
	this.aspect = 0;
	this.clearColor = 0;
	this.backgroundColor = 0;
	this.properties = null;
	this.mode = '';
	this.helpers = [];
	this.stats = null;
	this.workspace = null;
	this.rendererPiece = null
	
	this.onFirstComment = function(event) {
		var blocos  = Blockly.mainWorkspace.getAllBlocks();
		if (blocos.length > 0){
			var i;
			for (i = 0; i < blocos.length; i++){
				var bloco = blocos[i];
				if (bloco.type == "rendererpiece"){
					VisEdu.rendererPiece = new RendererPiece(bloco);
					VisEdu.rendererPiece.getGameObject();
					Game.apiHandler.properties = VisEdu.rendererPiece.properties;
					VisEdu.rendererPiece.update();
					var objetosGraficos =  bloco.childBlocks_;
					if (objetosGraficos.length > 0){
						var l;
						for (l = 0; l < objetosGraficos.length; l++){
							var bloco = objetosGraficos[l];
							if (bloco.type == "objetografico"){
			
								var groupGraphicalBehaviour = new GroupGraphicalBehaviour();
								if (bloco.graphicObjectPiece) {
									groupGraphicalBehaviour.removePiece(bloco.graphicObjectPiece, VisEdu.rendererPiece);
								}
									
								bloco.graphicObjectPiece = 	new GraphicObjectPiece(bloco);
								groupGraphicalBehaviour.addPiece(bloco.graphicObjectPiece, VisEdu.rendererPiece);
								var childBlocks =  bloco.childBlocks_;
								if (childBlocks.length > 0){
									var j;
									for (j = 0; j < blocos.length; j++){
										var childBlock = childBlocks[j];
										if (childBlock.type == "cubo"){
											
											var elementGraphicalBehaviour = new ElementGraphicalBehaviour();
											if (childBlock.cubePiece){
												elementGraphicalBehaviour.removePiece(childBlock.cubePiece, bloco.graphicObjectPiece);
											} 
			
											childBlock.cubePiece = new CubePiece(childBlock);
											elementGraphicalBehaviour.addPiece(childBlock.cubePiece, bloco.graphicObjectPiece);
										}
									}
								}
							}
						}
					}						
				}
			}
		}
	}
		
	
	this.setupFactory = function(mode) {
		var current = this.factory;
		if (mode == '3D') {
			this.factory = this.factory3D;
			this.setup3D();
		} else if (mode == '2D') {
			this.factory = this.factory2D;
			this.setup2D();
			
		} else {
			console.log('[VisEdu] no factory for mode: '+mode);
		}
		this.mode = mode;
		PropertiesController.check3DProperties();
		if (current && this.factory != current) {
			this.reloadScene();
		}
	}
	
	this.setup = function() {
		this.scene = new Scene().initialize(-1000, -1000, 1000, 1000);
		this.defLight = ThreeJSBuilder.createAmbientLightObject(0xFFFFFF);
		this.scene.addLight(this.defLight);
		this.root = new Layer().initialize();
		this.scene.addLayer(this.root);
	}
	
	this.loadStats = function() {
		this.stats = new Stats();
		this.stats.domElement.style.position = 'absolute';
		this.stats.domElement.style.bottom = '50%';
		this.stats.domElement.style.right = '0';
		this.stats.domElement.style.zIndex = 100;
		$(Game.apiHandler.getContext()).parent().append( this.stats.domElement );
	}
	
	this.afterLoad = function() {		
		VisEdu.axis = ThreeJSBuilder.createAxisObject(0, 0.1, 0, 450);
		VisEdu.grid = ThreeJSBuilder.createGridObject(0, 0, 0, 400, 20);
		Game.apiHandler.addGameObject(VisEdu.axis, this.scene);
		Game.apiHandler.addGameObject(VisEdu.grid, this.scene);
	}
	

	this.setup2D = function(camera) {
		Game.apiHandler.controls.reset();
		Game.camera.threeObject.position.set(0, 0, 1200);
		Game.camera.threeObject.lookAt(new THREE.Vector3(0, 0 ,0));
		Game.apiHandler.controls.noRotate=true;
	}

	this.setup3D = function(camera) {
		Game.apiHandler.controls.reset();
		Game.camera.threeObject.position.set(0, 500, 1200);
		Game.camera.threeObject.lookAt(new THREE.Vector3(0, 0 ,0));	
		Game.apiHandler.controls.noRotate=false;
	}
	
	this.create = function (world, blocklyDiv) {
		var toolbox = '<xml>';
		toolbox += '	<category name="Comandos Basicos" expanded="false" colour="150">';
		toolbox += '		<category name="Laco" colour="120">';
		toolbox += '			<block type="controls_whileUntil"></block>';
		toolbox += '		</category>';
		toolbox += '		<category name="Texto" colour="150">';
		toolbox += '			<block type="text"></block>';
		toolbox += '			<block type="text_join"></block>';
		toolbox += '		</category>';
		toolbox += '		<category name="Logica" colour="210">';
		toolbox += '			<block type="controls_if"></block>';
		toolbox += '			<block type="logic_negate"></block>';
		toolbox += '			<block type="logic_compare"></block>';
		toolbox += '			<block type="logic_operation"></block>';
		toolbox += '			<block type="logic_boolean"></block>';
		toolbox += '		</category>';
		toolbox += '		<category name="Matematica" colour="230">';
		toolbox += '			<block type="math_number"></block>';
		toolbox += '			<block type="math_arithmetic"></block>';
		toolbox += '			<block type="math_number_property"></block>';
		toolbox += '		</category>';
		toolbox += '		<category name="Variaveis" colour="330">';
		toolbox += '			<block type="variables_set" colour="330"></block>';
		toolbox += '			<block type="variables_get"></block>';
		toolbox += '		</category>';
		toolbox += '	</category>';
		toolbox += '	<category name="Objetos Gráficos" expanded="false" colour="150">';
		toolbox += '		<block type="objetografico"></block>';
		toolbox += '		<block type="cubo"></block>';
		toolbox += '	</category>';
		toolbox += '</xml>'; 
		
		this.workspace = Blockly.inject(blocklyDiv, {toolbox : toolbox});
		this.workspace.addChangeListener(this.onFirstComment);
		var xml = '<xml><block type="rendererpiece" deletable="false" movable="false">';
		xml += '<field name="clear">#333333</field>';
		xml += '<field name="background">#000000</field>';
		xml += '<field name="grid">TRUE</field>';
		xml += '<field name="axis">TRUE</field>';
		xml += '<field name="mode">3D</field>';
		xml += '<field name="BOOL">TRUE</field>';
		xml += '</block></xml>';
	    Blockly.Xml.domToWorkspace(this.workspace, Blockly.Xml.textToDom(xml));
		Game.loadAPI(new ThreeJSCustomHandler());
		window.addEventListener( 'resize', this.onResize, false );
		VisEdu.aspect = world.width() / (world.height()/2);
		this.setup();
		Game.init(world, this.scene);
		this.setupFactory('3D');
		this.afterLoad();
		
	}
	
	this.onResize = function (){
		var target = $('.world');
		VisEdu.aspect = target.width() / (target.height()/2);
		VisEdu.setupCamera(Game.camera.threeObject, target);
		var auxCam = Game.apiHandler.auxCamera;
		if (auxCam) {
			VisEdu.setupCamera(auxCam.threeObject, target);	
			var camPiece = $('.piece.camera.element').data('piece');
			camPiece.type.graphicalBehaviour.reloadPiece(camPiece);
		}
	    Game.apiHandler.renderer.setSize( target.width(), target.height() );
	}
	
	this.reloadProperties = function (properties) {
		this.properties = properties;
		this.checkURS();
		this.backgroundColor = properties['background'];
		this.clearColor = properties['clear'];
		this.setupFactory(properties['mode']);
	}
	
	this.hideURS = function() {
		if (VisEdu.axis && VisEdu.grid) {
			VisEdu.axis.threeObject.visible = false;
			VisEdu.grid.threeObject.visible = false;	
		}
	}
	
	this.checkURS = function() {
		if (this.properties) {
			VisEdu.axis.threeObject.visible = this.properties['axis'];
			VisEdu.grid.threeObject.visible = this.mode === '3D' && this.properties['grid'];
		} else {
			VisEdu.hideURS();
		}
	}
	
	this.setupCamera = function (camera, target) {
		camera.aspect = VisEdu.aspect;
	    camera.updateProjectionMatrix();
	    
	}
	
	this.addHelper = function(helper) {
		VisEdu.scene.threeObject.add(helper);
		this.helpers.push(helper);
	}
	
	this.removeHelper = function(helper) {
		VisEdu.scene.threeObject.remove(helper);
		$.each(this.helpers, function(index, item) {
			if (item === helper) {
				delete VisEdu.helpers[index];
			}
		});
	}
	
	this.hideHelpers = function() {
		$.each(this.helpers, function(index, item) {
			if (item) {
				item.visible = false;
			}
		});
	}
	
	this.showHelpers = function() {
		$.each(this.helpers, function(index, item) {
			if (item) {
				item.visible = true;
			}
		});
	}
	
	this.reloadScene = function() {
		var renderer = $('.piece.renderer');
		var json = PiecesUtils.genJSON(renderer);
		
		var node = $('.renderer + .object-node')
		$.each(node.find('> .connector.busy'), function(index, item) {
			var piece = $(item).find('> .piece.element').data('piece');
			piece.type.treeBehaviour.removePiece(piece);
		});
		
		PiecesUtils.readJSON(renderer, json);
	}
}